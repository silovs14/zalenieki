    <div class="footer w-100">
        <div class="forest-footer" style="background-image: url('./images/bg/footer-forest.png' )"></div>
            <div class="footer-dark">
                 <div class="content-wrapper">
                     <!-- top -->
                     <div class="top">
                             <h2 class="mr-auto ml-0">Rekvizīti</h2>
                         <div class="d-flex column-mobile-footer">
                             <div class="w-100">
                                 <div class="d-flex flex-item-footer">
                                    <div >
                                        Z/S “Zaļenieku kokaudzētava”<br>
                                        Reģ.nr.: LV43601022123<br>
                                        AS SEB banka, Jelgavas filiāle <br>
                                        Kods: UNLALV2X008 <br>
                                        Konts: LV28UNLA0008007643435
                                    </div>

                                 </div>
                             </div>

                             <div class="d-flex flex-item-footer flex-column align-items-center w-100">
                                 <div>
                                    <div class="single-contact-info-footer position-relative"><div class="green-wrapper-footer position-absolute"> <img src="images/icons/place.svg" alt=""> </div> <a href="">Kokaudzētavas iela 1, Zaļenieki, Zaļenieku pagasts, Jelgavas novads, LV- 3011, Latvija </a></div>

                                    <div class="single-contact-info-footer  position-relative"><div class="green-wrapper-footer position-absolute"> <img src="images/icons/phone.svg" alt=""> </div> <a href="tel:26359184">26359184 </a>;&nbsp; <a href="tel:63074444">63074444</a></div>

                                    <div class="single-contact-info-footer  position-relative"><div class="green-wrapper-footer position-absolute"> <img src="images/icons/mail.svg" alt=""> </div> <a href="mailto:kokaudzetava@zalenieki.lv">kokaudzetava@zalenieki.lv</a></div>
                                 </div>
                             </div>

                             <div class="d-flex flex-item-footer mr-0 ml-auto w-100 flex-column align-items-end">
                                 <div>
                                     <h2>Seko mums</h2>
                                         <div class="d-flex social-icons-footer">

                                             <a href="#" class="icon-shake" target="_blank"> <img src="images/icons/f-1.svg" alt=""></a>
                                             <a href="#" class="icon-shake" target="_blank"> <img src="images/icons/f-2.svg" alt=""></a>
                                             <a href="#" class="icon-shake" target="_blank"> <img src="images/icons/f-3.svg" alt=""></a>
                                             <a href="#" class="icon-shake" target="_blank"> <img src="images/icons/f-5.svg" alt=""></a>
                                             <a href="#" class="icon-shake" target="_blank"> <img src="images/icons/draugiemlv-ikona.svg" alt=""></a>

                                         </div>
                                 </div>
                             </div>
                         </div>

                     </div>


                    <!-- bottom -->
                     <div class="bottom d-flex justify-content-between">
                         <div>
                             <h2  class="green-title-footer">Zaļenieku kokaudzētava</h2>
                         </div>
                         <div class="d-flex  align-items-center">
                        <div class="cookies" >
                             <ul>
                                 <li><a href="#">Politika</a></li>
                                 <li><a href="#">Sīkdatnes</a></li>
                             </ul>
                        </div>
                             <div class="developed-by-wrapper">
                                <ul>
                                     <li>
                                         <a href="https://grandem.lv/" target="_blank" class="d-flex align-items-center">Web izstrāde: <div class="developer-by-logo" target="_blank"> <img src="images/icons/grandem-logo.svg" alt=""> </div> </a>
                                     </li>
                                </ul>
                             </div>

                         </div>
                     </div>

                 </div>
            </div>

    </div>
</body>
</html>