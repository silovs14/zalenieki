<?php require_once "header.php"; ?>

    <div class="contacts default-page-min-height">

        <!-- HERO  -->
        <?php require "included/inc_hero.php"; ?>
        <!-- HERO END -->

        <div style="background-image: url('./images/bg/landing-bg.png');">
            <div class="content-wrapper">

                <div class="contact-us-info d-flex flex-wrap">

                    <div class="single-contact-element prevent-shaking-animation">
                        <a href="#">
                            <div class="logo-wrapper d-flex flex-column">
                                <img src="images/icons/address-icon.svg" alt="" class="contacts-logo">
                            </div>
                            <div>
                                <h2 class="title-contacts">Adrese</h2>
                                <p>Kokaudzētavas iela 1, Zaļenieki, Zaļenieku pagasts, Jelgavas novads, LV- 3011,
                                    Latvija</p>
                            </div>
                        </a>
                    </div>

                    <div class="single-contact-element prevent-shaking-animation">
                        <a href="mailto:kokaudzetava@zalenieki.lv">
                            <div class="logo-wrapper d-flex flex-column">
                                <img src="images/icons/email-icon.svg" alt="" class="contacts-logo">
                            </div>
                            <div>
                                <h2 class="title-contacts">E-pasts</h2>
                                <p>kokaudzetava@zalenieki.lv</p>
                            </div>
                        </a>
                    </div>

                    <div class="single-contact-element prevent-shaking-animation">
                        <div class="logo-wrapper d-flex flex-column">
                            <img src="images/icons/call-icon.svg" alt="" class="contacts-logo">
                        </div>
                        <div>
                            <h2 class="title-contacts">Zvaniem</h2>
                            <a href="tel:(+371)63074444" class="d-flex padding-r-10 flex-wrap"><p>Tirdzniecība:</p>
                                <p class="number"><strong>(+371)63074444</strong></p></a>
                            <a href="tel:(+371)29410756" class="d-flex padding-r-10 flex-wrap"><p>Tirdzniecība:</p>
                                <p class="number"><strong>(+371)29410756</strong></p></a>
                            <a href="tel:(+371)26359184" class="d-flex padding-r-10 flex-wrap"><p>Vadītājs:</p>
                                <p class="number"><strong>(+371)26359184</strong></p></a>
                        </div>
                    </div>

                    <div class="single-contact-element prevent-shaking-animation">
                        <div class="logo-wrapper d-flex flex-column">
                            <img src="images/icons/time-icon.svg" alt="" class="contacts-logo">
                        </div>
                        <div>
                            <h2 class="title-contacts">Darba laiks</h2>


                            <span class="d-flex padding-r-10"><p>P-S.:</p>  <p><strong>08:00 - 17:00</strong></p></span>
                            <span class="d-flex padding-r-10"><p>Sv.:</p>  <p><strong>09:00 - 14:00</strong></p></span>
                        </div>
                    </div>
                </div>

                <div class="contacts-image">
                    <img src="images/contacts/contacts-image.jpg" alt="">
                </div>
            </div>
        </div>

        <div class="map-container d-flex  position-relative">
            <div class="map-wrapper" id="map">
                <script async defer
                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCT4RE5pXu24u295l4wACmnHCfsp4fD9C0&callback=initMap"
                        type="text/javascript"></script>
                <script>
                    function initMap() {
                        let coordinates = {lat: 56.968820, lng: 21.976490};
                        // MAP INITIALIZING
                        let map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 16,
                            center: coordinates,
                            gestureHandling: 'cooperative',
                            zoomControl: true,
                            mapTypeControl: false,
                            streetViewControl: false,
                        });
                        // END MAP INITIALIZING
                        // MARKER
                        let marker = new google.maps.Marker({
                            position: coordinates,
                            animation: google.maps.Animation.DROP,
                        });
                        marker.setMap(map);
                        // END MARKER
                    }
                </script>


            </div>


                <div class="d-flex map-nav-buttons position-absolute d-flex align-items-center">
                    <a href="#" class="prevent-shaking-animation"><img src="images/icons/waze.svg" alt=""></a>

                    <a href="#" class="prevent-shaking-animation"><img src="images/icons/google_maps.svg" alt=""></a>

                </div>

        </div>


        <div class="content-wrapper ">
            <div class="d-flex form-wrapper">
                <div class="form-contacts w-100">
                    <form action="#" name="contact-form" id="contact-form" novalidate>

                            <div class="single-input warning-validation">
                                <label for="name-contacts">
                                    <img src="images/icons/person-icon-green.svg" alt="">
                                </label>
                                <input placeholder="Vārds" id="name-contacts" type="text">
                            </div>

                            <div class="single-input">
                                <label for="email-contacts">
                                    <img src="images/icons/mail-icon-green.svg" alt="">
                                </label>
                                <input placeholder="E-pasta adrese"  id="email-contacts" type="text">
                            </div>

                                <textarea name="message" id="" placeholder="Ziņa"></textarea>

                        <input class="button button-black prevent-shaking-animation button-hover-shadow w-100" type="submit" value="Nosūtīt">
                        <div class="position-relative">
                            <img src="images/icons/preloader.png" class="preloader preloader-subscribe position-absolute">
                            <div class="warning-message non-valid-email-warning w-100" style="display: none;">E-pasts nav derīgs</div>
                            <div class="warning-message empty-field-warning w-100" style="display: none;">Nav ievadīta visa nepieciešāmā informācija</div>
                        </div>
                    </form>

                </div>

                <div class="w-100">
                    <div class="form-contacts-paragraph ">
                        <p>Zaļenieku kokaudzētava atrodas Latvijas vidusdaļā, Jelgavas novadā, Zaļenieku ciema centrā – 65
                        km attālumā no Rīgas, 21 km no Jelgavas, 19 km no Dobeles, 8 km no Tērvetes. No Rīgas līdz pašai
                        Zaļenieku kokaudzētavai ir asfaltēts ceļš. Izbraucot no Rīgas ērtāk braukt pa Jelgavas
                        apvedceļu, neiebraucot pilsētā, tālāk Tērvetes virzienā. Braucot no Jelgavas puses caur Svēti,
                        3.6km attālumā no kokaudzētavas, autoceļa Jelgava-Tērvete- Žagare labajā pusē izvietots liels
                        kokaudzētavas reklāmas objekts. Iebraucot Zaļenieku ciema centrā, krustojumā ir divpusēja
                        norāde: Kokaudzētava 1 km.</p>
                    </div>

                </div>

            </div>
        </div>


    </div>

<?php require_once "footer.php"; ?>