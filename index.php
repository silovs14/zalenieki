<?php require_once "header.php"; ?>
    <div class="banner-opened">
        <?php require_once "page-components/popup-banner.php"; ?>
        <script>
            $(function () {
                $('#openBanner').click();
            });
        </script>
        <div class="d-flex" style="width: 0; height: 0; padding: 0; margin: 0; opacity:0;">
            <a href="" id="openBanner" data-toggle="modal" data-target="#banner" data-counter="1"
               style="width: 0; height: 0; padding: 0; margin: 0; opacity:0 ; ">banner test</a>
        </div>
    </div>
    <!-- HERO  -->
<?php require "included/inc_hero.php"; ?>
    <!-- HERO END -->


    <div class="background-image-slider" style="background-image: url('./images/bg/landing-bg.png');">
        <div class="content-wrapper d-flex justify-content-center">
            <h2 class="main-title-w-decor">Jaunumi</h2>
        </div>
        <div class="multiple-items content-wrapper">
            <?php
            $titles = ["Mūsu kokaudzētava", "Septembra atlaides skolēniem un senioriem", "Vasaras ekskursijas!"];
            $count = 1 + 6;
            for ($i = 1, $image = 1, $title = 0; $i < $count; $i++, $image++, $title++) {
                if ($image > 3) $image = 1;
                if ($title >= 3) $title = 0;
                require "page-components/single_news_component.php";
            } ?>
        </div>
    </div>

    <div class="api-container d-flex content-wrapper">
        <div class="w-100">
            <div class="d-flex w-100 justify-content-center"><h2 class="main-title-w-decor">Facebook</h2></div>
            <h2 style="margin: 0 auto">backend plugin</h2>

        </div>

        <div class="w-100">
            <div class="d-flex w-100  justify-content-center"><h2 class="main-title-w-decor">Instagram</h2></div>

            <div>
                <div id="instafeed">

                    <h2 style="margin: 0 auto">backend plugin</h2>

                </div>

            </div>

        </div>
    </div>


<?php require_once "footer.php"; ?>