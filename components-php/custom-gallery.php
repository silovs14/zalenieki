<div class="wysiwyg-gallery d-flex flex-wrap">
    <?php $imageCounter = 0;
    $count = count($attachments);
    foreach ($attachments as $id => $attachment) {
        if ($imageCounter > 3) break;
        $img = wp_get_attachment_image_src($id, 'medium_large');
        $alt = get_post_meta($id, '_wp_attachment_image_alt', true); ?>
        <div class="single-gallery-element position-relative overflow-hidden transition-default">
            <a href="" data-toggle="modal" data-target="#galleryImages<?= $funcCounter; ?>" data-counter="<?= $imageCounter; ?>" class="slider-image-link">
                <img src="<?= $img[0]; ?>" class="gallery-image position-absolute w-100 h-100" alt="<?= $alt; ?>">
                <?php if ($imageCounter == 3 && $count > 4) { ?>
                    <div class="darkness-effect position-absolute w-100 h-100"></div>
                    <div class="number-additional position-absolute"><?= "+" . ($count - 4) . " " . __("bildes", 'Tomorrow'); ?></div>
                <?php } ?>
            </a>
        </div>
        <?php $imageCounter++;
    } ?>
</div>

<div class="image-modal-window">
    <div class="modal" id="galleryImages<?= $funcCounter; ?>" tabindex="-1">
        <div class="modal-dialog d-inline-block w-100">
            <div class="modal-content d-block">
                <div id="imagesSlider<?= $funcCounter; ?>" class="carousel slide" data-ride="carousel" data-interval="false">
                    <div class="carousel-inner">
                        <?php $modalSliderActive = 0;
                        foreach ($attachments as $id => $attachment) {
                            $img = wp_get_attachment_image_src($id, 'modal-thumb');
                            $alt = get_post_meta($id, '_wp_attachment_image_alt', true); ?>
                            <div class="carousel-item <?php if ($modalSliderActive == 0) echo "active"; ?>" data-counter="<?= $modalSliderActive; ?>">
                                <div class="align-modal-helper d-flex flex-wrap align-items-center justify-content-center">
                                    <img src="<?= $img[0]; ?>" class="slide-image"  alt="<?= $alt; ?>">
                                    <?php $imageDescription = get_post($id);
                                    if ($imageDescription->post_content) { ?>
                                        <div class="w-100"></div>
                                        <div class="gallery-slide-description overflow-auto"><?= $imageDescription->post_content; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php $modalSliderActive++;
                        } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($count > 1) { ?>
            <a class="carousel-control-prev position-absolute" href="#imagesSlider<?= $funcCounter; ?>" role="button" data-slide="prev">
                <img src="<?php bloginfo("template_url"); ?>/images/icons/arrow-left-white.svg">
            </a>
            <a class="carousel-control-next position-absolute" href="#imagesSlider<?= $funcCounter; ?>" role="button" data-slide="next">
                <img src="<?php bloginfo("template_url"); ?>/images/icons/arrow-right-white.svg">
            </a>
        <?php } ?>
        <button class="modal-cross position-absolute" data-dismiss="modal">
            <img src="<?php bloginfo("template_url"); ?>/images/icons/cross.svg" alt="">
        </button>
    </div>
</div>