<?php require_once "header.php"; ?>

    <div class="about-us default-page-min-height">

        <!-- HERO  -->
        <?php require "included/inc_hero.php"; ?>
        <!-- HERO END -->


        <div class="white-container">
            <div class="content-wrapper wysiwyg-style">

                <img class="alignright" src="images/about-us/about-us-1.jpg" alt="">

                <h2>Īsumā par Zaļenieku kokaudzētavu</h2>

                <p>Zaļenieku kokaudzētava ir pilna cikla uzņēmums. Piedāvātais dekoratīvo augu sortiments ietver divas
                    galvenās augu grupas:</p>
                <ul>
                    <li>lapu koki, košumkrūmi, vīteņaugi, potēti koki un krūmi;</li>
                    <li>skuju koki un potēti skuju koki;</li>
                </ul>
                <p>Mēs piedāvājam pašu audzētu, kvalitatīvu, Latvijas klimatam piemērotu- tātad ziemcietīgu,augu
                    sortimentu konteineros. Pavasarī, rudenī iespējams iegādāties arī kailsakņu stādus (dzīvžogu
                    materiāls, tūjas, dižstādi, soliteri ).</p>


            </div>
        </div>


        <div class="grey-container">
            <div class="content-wrapper wysiwyg-style">
                <img class="alignleft" src="images/about-us/about-us-2.jpg" alt="">
                <br>
                <h2>Vēsture un Teritorija</h2>
                <p>Zaļenieku kokaudzētava dibināta 1995.gadā. Kokaudzētavas dibinātājs, īpašnieks un vadītājs ir
                    diplomēts dārznieks ar 30 gadu pieredzi – Imants Parfenovičs. Aizvadītajos gados pakāpeniski
                    palielināta ražošana, attīstīta infrastruktūra, ievērojami paplašinot audzēto kokaugu sortimentu.
                    Pateicoties neatlaidīgam un mērķtiecīgam darbam, kolektīva profesionalitātei, ir iekarota stipra
                    pozīcija Latvijas stādu tirgū. Patreiz Zaļenieku kokaudzētava ir viena no lielākajām un vadošajām
                    kokaugu audzētājām Latvijā. Kokaudzētava ir Latvijas Stādu audzētāju biedrības biedrs.
                </p>

                <p>
                    Kopējā platība 20 ha, no kuriem 15 ha ir stādus ražojošā platība. Liela siltumnīcu saimniecība, 5 ha
                    platībā – laukums konteinerstādu audzēšanai, 10 ha platībā augi tiek audzēti atklātā laukā.
                </p>
            </div>
        </div>

        <div class="white-container">
            <div class="content-wrapper wysiwyg-style">

                <h2>Specializējamies lapu koku potēšanā, košumkrūmu audzēšanā</h2>
                <p>Jau dibinot kokaudzētavu, tika plānots nodarboties ar dekoratīvo koku un krūmu audzēšanu,
                    specializējoties kokaugu potēšanā. Strādājam ar sekojošiem lapu koku un krūmu potējumiem: bērzs
                    (Betula), liepa (Tilia), goba (Ulmus), pīlādzis (Sorbus), vītols (Salix), kļava (Acer), osis
                    (Fraxinus), dižskābardis (Fagus), ābele (Malus), karagāna (Caragana), plūme (Prunus), lazda
                    (Corylus), ceriņš (Syringa), segliņš (Euonymus) u.c. Bez tā specializējamies arī lielāka izmēra lapu
                    koku audzēšanā. Esam izveidojuši nopietnas un interesantas kokaugu kolekcijas. Paši veicam spraudeņu
                    apsakņošanu no saviem mātesaugiem, audzējam lapu koku un krūmu jaunstādus. Piedāvājam lielu
                    pašaudzētu košumkrūmu dažādību, arī liela izmēra konteinetros. Papildus uzņēmums specializējas arī
                    lielāka izmēra lapu koku audzēšanā. Piedāvājam populārākās Latvijas sugas. Savus kokus mēs pavadām
                    visā audzēšanas ciklā – no kvalitatīvas vietējās sēklas, spraudeņa, potējuma, tad – jauna kociņa
                    līdz 3,5 m augstam kokam. Lielu uzmanību pievēršam arī jaunu un retu kokaugu ieviešanai. Jaunumi un
                    retumi ik gadu būtiski papildina mūsu sortimentu.</p>
            </div>
        </div>

        <div class="grey-container">
            <div class="content-wrapper wysiwyg-style">
                <h2>Tirdzniecība</h2>
                <p>Esam veiksmīgi attīstījuši ne tikai mazumtirdzniecību, bet arī stādu vairumtirdzniecību –
                    apzaļumošanas un celtniecības uzņēmumiem, pašvaldībām, dārzu centriem utt. Iepriekš vienojoties,
                    piedāvājam stādu piegādi pa visu Latviju.</p>

                <ul>
                    <li>Mazumtirdzniecību veicam atbilstoši <a href="#">Lapu koku/ krūmu un Skuju koku katalogam</a>
                    </li>
                    <li>Mūsu stādus var iegādāties kokaudzētavā Zaļeniekos un mūsu Rīgas filiālē – Labklājības dārzi, kā
                        arī pie mūsu sadarbības partneriem (pa pastu nesūtām);
                    </li>
                    <li>Pasūtījumus pieņemam rakstveidā caur formu mūsu mājas lapā www.zalenieki.lv (sortiments), vai pa
                        e-pastu kokaudzetava@zalenieki.lv, un tel. 26359184
                    </li>
                    <li>Lūdzam lielākus pasūtījumus veikt 2-3 dienas pirms ierašanās kokaudzētavā;</li>
                    <li>Kokaudzētava ir tiesīga sezonas laikā mainīt pārdošanā esošo stādu cenas.</li>
                </ul>
            </div>
        </div>

        <div class="white-container">
            <div class="content-wrapper wysiwyg-style">
                <h2>Dendrārijs</h2>

                <p>
                    Dendrārijs jeb iepazīšanās dārzs aizņem 1 ha platību, kurā apskatāmi vairāk kā 2000 kokaugu taksonu.
                    Dārzs ir atvērts apmeklētājiem kokaudzētavas darba laikā. Šeit var iepazīties ar lielāka izmēra
                    kokiem un krūmiem, ko varētu ieprojektēt (pārnest) arī savā dārzā. Pirms sākt pavairot kokaugus, tie
                    tiek novēroti un salīdzināti gan dendrārijā, gan citos kolekciju stādījumos. Dārzs gada laikā
                    pastāvīgi mainās. Viena un tā pati vieta savādāk izskatās pavasarī, vasarā, rudenī un ziemā. Dārzu
                    apmeklē ekskursijas no dažādām valstīm un tas tiek piedāvāts arī pasākumiem (kāzas, fotosesijas
                    utt.)
                </p>
            </div>
        </div>


    </div>

<?php require_once "footer.php"; ?>