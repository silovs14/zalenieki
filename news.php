<?php require_once "header.php"; ?>

<div class="news default-page-min-height">

    <!-- HERO  -->
    <?php require "included/inc_hero.php"; ?>
    <!-- HERO END -->

    <!--NEWS ELEMENT-->
    <div class="content-wrapper">
        <div class="news-grid d-flex flex-wrap">
            <?php
            $titles = ["Mūsu kokaudzētava", "Septembra atlaides skolēniem un senioriem", "Vasaras ekskursijas!"];
            $count = 1 + 6;
            for ($i = 1, $image = 1, $title = 0; $i < $count; $i++, $image++, $title++) {
                if ($image > 3) $image = 1;
                if ($title >= 3) $title = 0;
                require "page-components/single_news_component.php";
            } ?>
        </div>
        <div class="d-flex justify-content-center w-100">
            <nav aria-label="Page navigation">
                <ul class="pagination">
                    <li class="page-item">
                        <a class="page-link" href="single-news.php" aria-label="Previous">
                            <span aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" width="8" height="14" viewBox="0 0 8 14"><path  d="M6.293,13.707l-6-6a1,1,0,0,1,0-1.414l6-6A1,1,0,0,1,7.707,1.707L2.414,7l5.293,5.293a1,1,0,1,1-1.414,1.414Z" fill="#fff"/></svg></span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" width="8" height="14" viewBox="0 0 8 14"><path d="M1.707,13.707l6-6a1,1,0,0,0,0-1.414l-6-6A1,1,0,1,0,.293,1.707L5.586,7,.293,12.293a1,1,0,1,0,1.414,1.414Z" fill="#fff"/></svg></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <!--NEWS ELEMENT -->

</div>

<?php require_once "footer.php"; ?>