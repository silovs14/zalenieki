<?php require_once "header.php"; ?>

    <div class="news default-page-min-height">

        <!-- HERO  -->
        <?php require "included/inc_hero.php"; ?>
        <!-- HERO END -->

        <!--NEWS ELEMENT-->
        <div class="content-wrapper">

            <div class="wysiwyg-style ">
                <p class="date-created-post">Jūl 14, 2021</p>
                <h2>Īsumā par kokaudzētavu</h2>
                <p>Maecenas nec tempus urna. Nullam vel turpis vel augue volutpat rhoncus sed id sapien. Sed commodo
                    odio ac cursus porttitor. Sed in bibendum nisl. Duis commodo suscipit tincidunt. Pellentesque
                    dignissim elit non suscipit congue. Curabitur sed erat eu ipsum vehicula pulvinar eget at quam. Sed
                    maximus turpis eu justo viverra, quis viverra diam pellentesque. Pellentesque interdum lacus mollis,
                    pretium risus sit amet, suscipit nunc. Ut quis eros vel purus consequat dignissim. Aenean mattis
                    hendrerit blandit. Curabitur gravida, nulla at dictum scelerisque, urna felis malesuada enim, in
                    tincidunt mi augue auctor dui. Morbi sapien lectus, venenatis quis nisl ullamcorper, mattis semper
                    ante. Nulla ultrices ullamcorper volutpat.</p>
                <p>Praesent nec iaculis erat. Aenean ac metus porta, semper lectus a, pretium nunc. Fusce elit ligula,
                    aliquam maximus imperdiet sit amet, ultricies quis augue. Morbi fringilla, eros non rutrum molestie,
                    urna risus efficitur nisl, a eleifend augue neque sed dolor. Proin eleifend ex nisl, eget viverra
                    dolor vulputate vitae. Curabitur ac felis quam. In sed justo suscipit, ullamcorper mauris quis,
                    euismod nisl. Curabitur porttitor euismod velit ut lobortis.</p>
                <p>Donec dictum eget nulla id pulvinar. Vestibulum sed tempor justo. Fusce ligula metus, congue eu
                    pretium sed, ullamcorper eu leo. Suspendisse blandit scelerisque egestas. Nullam sagittis malesuada
                    vulputate. Nulla viverra dui eget quam maximus, sed ultricies risus luctus. Nunc eget ante eu ligula
                    vehicula varius. Sed ligula felis, tincidunt quis orci in, tincidunt dignissim eros. Sed vel lectus
                    sapien. Integer sit amet aliquet ex. Maecenas tristique metus leo, quis lobortis libero tincidunt
                    non. Morbi molestie massa vitae eros viverra, id consequat tellus porta.</p>

                <div class="position-relative img-wrapper-wysiwyg wysiwyg-style">
                    <img src="images/single-news-image/main-single.jpg" alt="">
                    <div class="image-author position-absolute">Foto: Jēkabs Vilkārsis</div>
                </div>
        <!-- auth text on image backend-->
                <div class="gallery-images-wrapper small-gallery">
                    <?php $count = 1 + 5;
                    for ($i = 1, $image = 1; $i < $count; $i++, $image++) {
                        if ($image > 3) $image = 1;
                        ?>
                        <a href="images/image.jpg" class="single-gallery-image prevent-shaking-animation" data-fancybox="galleryImages">
                            <img src="images/image.jpg" alt="">
                        </a>
                    <?php } ?>

                </div>


                <!--            gallery backend  -->


            </div>
        </div>
        <!--NEWS ELEMENT -->

    </div>

<?php require_once "footer.php"; ?>