<?php require_once "header.php"; ?>

<div class="partners default-page-min-height">

    <!-- HERO  -->
    <?php require "included/inc_hero.php"; ?>
    <!-- HERO END -->


    <div class="content-wrapper">
        <div class="d-flex">
            <h2 class="main-title green-title">Zaļenieku kokaudzētavas sadarbības partneri, pie kuriem var<br> iegādāties mūsu stādus.</h2>

        </div>

            <div class="partners-wrapper d-flex flex-wrap">

                <?php
                $titles = ["Labklājības dārzi SIA", "Madara Stūrmane IK", "Agroserviss Valmiera SIA", "Gartda SIA", "Sedumi SIA", "Kronis 93 SIA", "Siguldas Dārznieks SIA", "Z/S Īve (kokaudzētava)", "Silja SIA", "Sietiņš GO SIA", "Dārza laiks SIA (Interneta veikals)"];
                $count = 1 + 12;
                for ($i = 1, $image = 1, $title = 0; $i < $count; $i++, $image++, $title++) {
                    if ($image > 7) $image = 1;
                    if ($title >= 3) $title = 0; ?>

                <a href="#" class="single-partner-element prevent-shaking-animation">
                    <div class="logo-wrapper d-flex flex-column">
                        <img src="images/partners/p<?= $image; ?>.png" alt="" class="partners-logo">
                        <span class="green-line"></span>
                    </div>

                    <div>
                        <h2 class="title-partners"><?= $titles[$title]; ?></h2>

                        <div class="address-partners-wrapper" >
                            <p class="address-partners-card" >Vējkalni, Smārdes pag., Engures nov.</p>
                        </div>

                        <p class="tel-partners-card">22013936</p>
                    </div>
                </a>

                <?php  } ?>

            </div>

    </div>


</div>

<?php require_once "footer.php"; ?>