<!DOCTYPE html>
<html lang="lv">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <!-- <link rel="shortcut icon" type="image/png" href="./images/favicon.png"/> -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/slick-theme.css">
    <link rel="stylesheet" href="css/selectric.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/compiled-css/style.css">



    <script src="./js/jquery-3.3.1.js"></script>
    <script src="./js/jquery.selectric.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
    <script src="./js/slick.js"></script>
    <script src="./js/main.js"></script>
    <script src="./js/custom/slick-init.js"></script>


</head>
<body>

<div class="top-navbar" id="header-top">
    <div class="navbar-header position-relative">
    <!--MOBILE MENU -->
        <div class="mobile-background position-fixed transition-default mobile-background-hidden">
            <div id="dark-header-mobile"
                 class="mobile-menu mobile-menu-hidden dark-header w-100 position-absolute transition-default">
                <div class="position-relative mobile-wrapper position-fixed transition-default ">
                    <div class="mobile-header-wrapper w-100 ">
                        <div class="content-wrapper-mobile "><!--MENU-->
                            <div class="d-flex align-items-end flex-wrap justify-content-between w-100">

                            <div class="d-flex w-100 justify-content-end">
                                <div class="languages selectric-wrapper d-flex justify-content-end" >
                                    <select class="language-select selectric" style="opacity: 0">
                                        <option value="lv" selected >Latviešu</option>
                                        <option value="en" >English</option>
                                        <option value="ru" >Pусский</option>
                                    </select>
                                    <a  href="#"  class="shopping-cart " style="margin: 0px 20px" >
                                        <img src="images/icons/shopping-cart.svg" alt="">
                                    </a>
                                </div>
                            </div>
                                <div class="menu w-100 d-flex flex-column">

                                    <ul>
                                        <li><a href="#">Sākumlapa</a></li>
                                        <li><a href="#">Sortiments</a></li>
                                        <li><a href="#">Jaunumi</a></li>
                                        <li><a href="#">Par mums</a></li>
                                        <li><a href="#">Galerija</a></li>
                                        <li><a href="#">Kontakti</a></li>
                                    </ul>

                                </div>
                            </div>
                        </div><!--MENU END-->
                    </div>
                </div>
            </div>
        </div>
        <!--MOBILE MENU END-->

        <div class="d-flex align-items-center w-100">
            <nav class="navbar-default w-100 h-100 position-relative">
                <!--LOGO & BURGER-->
                <div class="mobile-header-wrapper w-100">
                    <div class="content-wrapper-mobile d-flex align-items-center flex-wrap justify-content-end w-100">
                        <!-- MAIN LOGO-->
                        <div class="mobile-logo-main align-items-center justify-content-start show-mob">
                            <a href="index.php"><img src="images/logo/main-logo.png" alt="logo"></a>
                        </div>
                        <!-- BURGER-->
                        <div class="mob-burger">
                            <button id="burger-mob" class="menu-opener collapsed burger d-block p-0" type="button">
                                <span class="d-block wrapper position-relative fadeButton">
                                <span class="line position-absolute d-block"></span>
                                <span class="line position-absolute d-block"></span>
                                <span class="line position-absolute d-block"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
                <!-- LOGO & BURGER END -->
                <!--DESKTOP-->
                <div class="header collapse navbar-collapse relative  h-100" id="header">
                    <div class="header-content content-wrapper d-flex flex-nowrap align-items-center justify-content-between">
                        <div class="main-info-desktop w-100">
                            <div class="menu justify-content-start">   <!--LOGO-->
                                <div class="logo d-flex align-items-center justify-content-start">
                                    <a href="index.php"><img src="images/logo/main-logo.png" alt="logo"></a>
                                </div>   <!--LOGO END-->

                            

                                <!--DESKTOP MENU-->
                                <div class="desktop-menu ul-wrapper position-relative d-flex">
                                    <ul>
                                        <li><a href="#">Sākumlapa</a></li>
                                        <li><a href="#">Sortiments</a></li>
                                        <li><a href="#">Jaunumi</a></li>
                                        <li><a href="#">Par mums</a>
                                            <ul class="sub-menu">
                                                <li><a href="#">Mūsu kokaudzētava</a></li>
                                                <li><a href="#">Sadarbības partneri</a></li>
                                                <li><a href="#">Rīgas stādu centrs</a></li>
                                            </ul></li>
                                        <li><a href="#">Galerija</a></li>
                                        <li><a href="#">Kontakti</a></li>
                                    </ul>
                                </div>

                                <div class="mr-0 ml-auto d-flex">

                                    <a  href="#"  class="shopping-cart">
                                        <img src="images/icons/shopping-cart.svg" alt="">
                                    </a>


                                    <div class="languages selectric-wrapper languages-desktop">
                                        <select class="language-select selectric" style="opacity: 0">
                                            <option value="lv" selected >Latviešu</option>
                                            <option value="en" >English</option>
                                            <option value="ru" >Pусский</option>
                                        </select>
                                    </div>

                                </div>

                            </div>
                           

                        </div>
                    </div>
                </div>
                <!--DESKTOP END-->
            </nav>
        </div>

    </div>
</div>