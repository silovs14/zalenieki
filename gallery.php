<?php require_once "header.php"; ?>

<div class="gallery-page default-page-min-height">

    <!-- HERO  -->
    <?php require "included/inc_hero.php"; ?>
    <!-- HERO END -->
    <div class="content-wrapper">
        <div class="d-flex d-flex-wrapper-gallery">
            <div class="gallery-category-wrapper">
                <div class="d-flex justify-content-end w-100 mob-hide ">
                    <button class="button green button collapsed menu-button" type="button" data-toggle="collapse" data-target="#category">
                        <span class="d-block burger-wrapper">Kategoriju saraksts<img src="images/icons/dd-icon.svg" class="dropdown-icon" alt=""> </span>
                    </button>
                </div>
                <div class="sidebar gallery-category-sidebar sidebar-content collapse navbar-collapse" id="category"  >
                    <ul>
                        <li><a href="#">Iepazīšanas dārzs pavasarī</a></li>
                        <li><a href="#">Iepazīšanas dārzs ziemā</a></li>
                        <li><a href="#">Iepazīšanas dārzs vasarā</a></li>
                        <li><a href="#">Stādu tirzniecība</a></li>
                        <li><a href="#">Iepazīšanas dārzs rudenī</a></li>
                        <li><a href="#">Skolnieki aplūko mūsu dārzus, un palīdz veikt uzkopšanas darbus</a></li>
                    </ul>
                </div>
            </div>

            <div class="gallery-images-wrapper">
                <?php $count = 1 + 8;
                for ($i = 1, $image = 1; $i < $count; $i++, $image++) {
                    if ($image > 3) $image = 1;
                    ?>
                    <a href="images/image.jpg" class="single-gallery-image prevent-shaking-animation" data-fancybox="galleryImages">
                        <img src="images/image.jpg" alt="">
                    </a>
                <?php } ?>

            </div>




        </div>
    </div>


</div>

<?php require_once "footer.php"; ?>