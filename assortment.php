<?php require_once "header.php"; ?>

    <div class="assortment default-page-min-height">

        <!-- HERO  -->
        <?php require "included/inc_hero.php"; ?>
        <!-- HERO END -->


        <div class="content-wrapper">
            <div class="top-wrapper">
                <div class="d-flex justify-content-between align-items-center top-title-wrapper">
                    <h2 class="main-title-h2">Mazumtirdzniecības cenu katalogs 2021. gads</h2>
                    <div><p class="green-color">Cenu katalogs atjaunots: 01.03.2021</p></div>
                </div>


                <div><p class="red-color sourceSansPro-bold ">Norādīto stādu sortiments un cenas var būt mainīgas</p>
                </div>
                <!-- grey-container-start-->
                <div class="grey-container-assortment">
                    <div class="border-bottom">
                        <p>Vairumpircējiem darbojas atlaižu formula – līdz 30% <br>
                            Sīkākai informācijai lūdzam zvanīt: 26359184 vai rakstīt uz e-pastu: <a
                                    href="mailto:kokaudzetava@zalenieki.lv">kokaudzetava@zalenieki.lv</a></p>
                    </div>
                    <div class="d-flex align-items-center excel-download">
                        <p>Viss katalogs EXCEL (.xlsx) formātā pieejams šeit…</p> <a href="#" class="icon-shake"><img
                                    src="images/icons/excel-icon.svg" alt=""></a>
                    </div>
                </div><!--grey-container-end-->

            </div>


            <div class="assortment-table border-bottom">
                <div class="table-filtration d-flex justify-content-between align-items-center">

                    <div class="abc-wrapper d-flex flex-wrap ">
                        <?php $AZ_Range = range('A', 'Z');
                        foreach ($AZ_Range as $letter): ?>
                            <a href="#" class="d-flex align-items-center justify-content-center"><?= $letter; ?></a>
                        <?php endforeach; ?>
                    </div>

                    <div class="search-window-wrapper">
                        <form action="" method="post" class="search-form" id="search-form" novalidate>
                            <div class="d-flex align-items-center flex-wrap-mobile">
                                <!--Checkbox New-->
                                <div class="checkbox-single w-100">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="privacy-child">
                                            <input type="checkbox" name="assortmentNew" id="assortmentNew"
                                                   class="position-absolute privacy-checkbox">
                                            <label for="assortmentNew"
                                                   class="d-inline-flex checkbox-label align-items-center">
                                                <div class="checkbox-custom-wrapper d-flex justify-content-center">
                                                    <div class="checkbox-back position-relative">
                                                        <div class="checkbox-mark position-absolute w-100 h-100 d-flex justify-content-center align-items-center">
                                                            <img src="images/icons/check-mark.svg" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="checkbox-title wrap red-color">Jaunums</div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <!--Checkbox New END -->

                                <!--Checkbox Edible-->
                                <div class="checkbox-single w-100">
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="privacy-child">
                                            <input type="checkbox" name="assortmentEdible" id="assortmentEdible"
                                                   class="position-absolute privacy-checkbox">
                                            <label for="assortmentEdible"
                                                   class="d-inline-flex checkbox-label align-items-center">
                                                <div class="checkbox-custom-wrapper d-flex justify-content-center">
                                                    <div class="checkbox-back position-relative">
                                                        <div class="checkbox-mark position-absolute w-100 h-100 d-flex justify-content-center align-items-center">
                                                            <img src="images/icons/check-mark.svg" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="checkbox-title wrap green-color">Ēdams</div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <!--Checkbox Edible END -->

                                <div class="search-window d-flex">
                                    <input type="text" name="search" id="search_info_in_table">
                                    <a type="submit" class="magnifier-search-button search-button">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                             viewBox="0 0 20 20">
                                            <path id="Icon"
                                                  d="M19,20a1,1,0,0,1-.613-.21l-.094-.083-3.676-3.675A8.9,8.9,0,0,1,9,18a9,9,0,1,1,9-9,8.9,8.9,0,0,1-1.967,5.617l3.675,3.676A1,1,0,0,1,19,20ZM9,2a7,7,0,1,0,7,7A7.008,7.008,0,0,0,9,2Z"/>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>


                <div class="table-wrapper wysiwyg-style">

                    <div class="assortment-table-wrapper  wysiwyg-table-wrapper">


                        <table class="assortment-table " style="width:100%">
                            <tr>
                                <th>N.p.k</th>
                                <th>Nosaukums</th>
                                <th>Piezīmes</th>
                                <th>Izmēri, cm <br>(augstums, platums)</th>
                                <th>Kont.liel. <br>(litri)</th>
                                <th>Cena ar PVN <br>(EUR)</th>
                                <th></th>
                            </tr>

                            <?php
                            $count = 1 + 13;
                            for ($i = 1;
                            $i < $count;
                            $i++) { ?>

                            <tr>
                                <td><?= $i; ?></td>

                                <td>
                                    <div class="d-flex align-items-center">
                                        <a href="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTExMWFhUXGBoaGBgYGBcaHRobHxodGhgeIB0dHSggGhsnHR8dITEhJSkrLi4uHR8zODMtNygtLisBCgoKDg0OGxAQGy0mICYyNzMvLS0tLS0tLzIwLS8vLy8tLS0tLS0vLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIALIBHAMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAFBgMEBwACAf/EAEEQAAECBAQDBgQDBgQGAwAAAAECEQADBCEFEjFBBlFhEyJxgZGhMkKxwRTR8AcjUnLh8TNikrIkNGNzgqIVQ8L/xAAbAQADAAMBAQAAAAAAAAAAAAADBAUBAgYAB//EADoRAAEDAgMECQQBAwMFAQAAAAECAxEAIQQxQRJRYXEFE4GRobHB0fAiMuHxFAYjMzRCUnKCorLCNf/aAAwDAQACEQMRAD8AdMGrlVKQClr+rRJVT5K5mScGSkHXR4lTQJpcvZkpS7XvrFDGcJVNSsJIJBCgT7xz7aEK2VITdJII36jwvaKvh1lT8TsJOWcideyqNMEonESpjyzcB7CO45l5US82j6+UDsIw4hZEwkJCtebQX4gqkqypXlVLCv7QZYQopUlN7zwp7+QgYtHVq24GmZt4mhOCFK1KEtF1JZn94P0WD/hpebObB1DaKHD2RKiZSCq7eHKHSkJyOtLHcRsGlOkBNgJkxroONriOe6g9JY1SFbMGDFrSfOKGYPicuYLK02ifFKvIjOi7aiAFZLH4qYuUxGUWFr7x4o8QJQVKScr97pAFYXbWWwoRrntWvI4GhKw6FLCkawdk53Ex6TV6jxBQStS0nnYEgcrxXqOJGBZJeL+HYpKSky1EXNhzB0gBxbSSpcsGVse9d4wvCNqSklXZ6VthVNOPhDyNknLd8taixcSjMUntCb2LtBPD6xMyUglri8L/AAFMK5UxKg6X7pPuImmTJRK5ctTKcsBsd4DiEO2KIk8xbKDu0yrL7Y21tZ7JmRlBG68VJiAkks9wbHlFrCMilKck5bB9IQqlM6WVdo4IOp3HOGbglRmdoCe6wMFU0Sn6e7lr7/inMRhAjDFxK5G+iM1MmVOVMBLqYHlBpCwUukWPKBnZypg7MiynBME0y0SJQSn4Ui93gKmlIaDqjEX5gxEDhFS31AhIuVcdRp7UHn4r2IXnDcvCB1TjYGUJBUV+sXaxKZ57gCibP03gvTUUpCR3EuGu0eZQCjaV2Tl2edYafbaMFBMZyY+XqliE7spaVy0KKmFgDePeF4smcgKPdUCxB5wTpqnMcrQMVhS1Kmp7iQVOCznSNQ2pSJ0mMojh+fOvIU0UlDliLzOk5RrRZM4HlFCul5iACGfQwNrKKamXMaa60hwBvFPh9c7uqmkd4sEq1aNg26NnriABJFyfIdhFbIw6dgrSsWtGu+3zhUVJh6fxBMtBWymJfug7w0VyghBUpI7oj1S0YlqUUn4tthAziKqQuTNQSXYi0DfXtwkHl81j0rLjvXupiSLDWdK8cNY6qpBGVm5aQfzpd4TOEqSd2B7EoSHIu79YLVc5aUsZS7aq5/nDqMGYUpRj/jJsd0ajjRcVh2ziFJaIF4gG450TxNKZiCBrsRq8DKionSZAUUhTDvXNhFnD0Mys3dZ784rYtVlY7IAHO/h5wopl1MFYBndl6Gl8OpJUEC6Zkz43q1hVaFyUqKWe7RdQhK090AeUAsMJdlkMNhE9ZiolMGPSNcS2cOoASoRuvesvNfWQnW+dXjg6FAKKUhSTYsIkJD5Slm94rYbigUDm7pGx5RJWAzE5kqsm569Iy2es2WU5m8Z8zQyFpVsuGw8KgxJE0yz2OUEaPCjUYzOMx5jGYgMyd3/tFzE62atKxIzHre3MCBfBq0Z1GeSFKNn+8MM4fqUuIJkTETnYXO4DzFWcLh0BhTiglUZACVaeFNGK0FWZIWgiwzEb6QKoptWECyi99OcPCMQSEsSIoVmKICm6RspjDtNJ2jPOCfeKjpxC3B1fVi2okW4xnQ7GMSzSwnKSTe12A3gTSYq4y5xoXO8MWLyhLkT+yQXWlgEhze0ZbMWUKSC4JNwbNGOrSUpSDJi/Pu+TVLA4RGMw5Qq0HQ8JNtBpWgUlFNnSitKXGx0e+0TysEExCUTRlWk5ibc/eKvCGNJRIKFKFlFvDf3jzPxkqmEgskkB+kbuqRsJWidoaTuqM6DhMWUgxBgHy8LUVpsLRJeYFG6rizchBVc1xfQxUoqXtQoZiEwExSqmyFlJuzMeY2PSF3y+tIeSmxF7gXnPtrDrqQ4etVflwsK+Vk1EusS9kqR3m6EtFbi3GqfJ2SFELa2QBr8+cDcLwqdUTJk1SibsCQwPQHpFCdh8uTUBU0kqQQWDEEbQzhVqaQEnOBf0nviqGHdbfxTewlSglGYt9QOvDIfIq9RVMsLQpd2DAmIK45lq/hUfKJJ1TTzVPlygkMeXO0WcQw9qXtpJKy7kN8oGsbFhRUTM/PzSaujnkvIGIMIUrOcpm05DdnHKjvD89JkCQgMRqRzMWqzhtJUlaDkKQ1t/6wl4BXzJYJY97aHeXjaCAXvCmIxOyqVcgR5UZZTh8QpGHXI1vPA8DxqnS4J2wV2wcA5b3doIYlRy5NOsSEBBYFkjW8U8MxjMVpQknvE+sSnEFJnErSQMu43hrCPpU4ptdpFyRvGk7xQS6459YVlcDQwQIilidioSlBe4U7ed/aC9LUTahKmDJ5GzwNxelQoFcxKQXcNzMXsKxtKBlUwADDrC+JbKU7CgbGfg5VvicShK0tK+68mRBm8dhoRh+Krk1LAZQ+XL5wVrsfX2hAS5cBhzgbW4d20xVRLSop2OxVppBGlwqbIImzU7ZrXazX6x5QLrO1BIFznFGfx2FceSEoM7IHCQfEETfjUVfxWpDFADtd9ovYJxKZoJUO9m25NAbE8AEwLUlbTDcDYveL/DGBJQhLqJU/eP26RjEGGQDInKL6TR3FYT+KNlH1mMwe2+WRt3UbWFzlOhJ0vA3Fqaaupp0oCR2Zcg7wzyVZbAW6RQxRaZaxNIuH+kEbc61Ab2juMgZQZgxuHfekA84lY6sCwMa5iD3ivXbnP2YHe5QLxOjPaBKkkJLknn0irRY/LVVZ7gKGW8HarEJazlKg3jGE4ZDUkqJO1AFhYXGVYebeQsJUCi0zrx130o0qZkmcLlKM4BY2h6XPSQzg9IVMcxSn70pKFFT6vYdY+4dXpEsqLZto1xgIjq7pNuXHuprGqcWlLjjeyALG17WkaTodaJcRYQlFMtSVqzAZtfNoEcFrKpE4qLsrunkGizV4pMnAykJzFQb84puqSRKUoS3S2UbjnD7IQfqaT9MeQ3mgYbEKVh14aRY7UnheB3X3TUq6uWpRKFZdidv7x6p62WqaAsgkaHrEWO9nKpVIQQokgvbeElNQykh77xjEH+QFIIggm4MxaKt4XCN45raBMTbLTWtHxKnRN+fKSHcdNoGTcQXKlqEtRHI9d4PYXhwMmUZku+UEP1F3i2nB6dQOZAPRy0KYdtS1hsWULzlutGdc2+44hWcpScsx8tSNwpjMxfaoWSCxYsRe7wLVRrM4hiA7lR5PGkow6UiWooSNSfSF2nIUuYtVyLJB0EEfbU2qRmq3AfM6osdLL69RaQE7WW4QKt0hkqyJQvNMca8t4JTcJzF1G8B52JIkjvoAKi4YR7wybUzkZ0pLOQLjaNSkI2kKSZB0sIikXkP4ZQWSYVNxwphEpSg4PIwncYcHzZ60zZYbu98ZgHINi3Noe5NkgcgB4sI9KXzhNLLbSusQTMG827QZojGKdZVtIj53VnHD+DrKFDs1FjyNjy8YnqcPmpYiWoJFjY2h3NSlG4HT7wv1fFKXVL+I3DD2h9tCC2layQTuj5FDx74xRVtJSDMzrYZcalwSsWiSSW1s5a2kUJ0tVXVKlpIsACdgNSfeAtRiKnAUe65YQZ4GnpE6YrQlFutw8bNL63ZbVlz3etIs4Z19lTiR9KMydcrDvo1xHiaKVKZKbfu3BA0YsPW/pGcVxExfaZi5JJEHP2hVbz0ga9mw9VGFZC8qbm+/3gmI++R2cuWvzlXbdD4JDWHS6n7lC/jVuXTPvc3T1a8M6qr8PJMoJOcoy62c3gVLmI/DSSpu07UhJ3KMofxDwyUeDpqJOcqVnu3Lu6eNoFh1KUdlGhI8Yt2VP/AKiU6WAE5BRnxA+bzwv5wTAcqc81lBgQAem8M6sKlGT2YSEkhwRqCYGSAUyAFatcdIqTOJkJbWw+0Dw2IbbSdtJMm9ptl6Vz/UICEpTn45e5qDh6rRJnTJS7KKmBPMbQU4jmgylNrZvWBlLSyJijULBUpZzAPYCzW948SqZM1yVqASQQAbPGuJacSErEQTJzn4edUcVh0IT/AGUkECDuB555+NWpfDSAxmrUsPe+h2bpERopBZkaHV+sT4Xiip0wSVBu9cjprBrEaNHZqKEAKFw2p5iGXkKdlSNN8ybXg5W4xSOGfQpX9y5MCc++arYdLCMqZaT2Y3OgOpi7istS5a2/hNtz4R4owUoD+cWjMgTWI6kqRaJmBlxjhw96ypICwU6ZdlZ7MMzMknuse8Dq+hHlDPhykyHQtYdbKHQ7x4x6lCpaikAFzNV1IT/aM6mVanzEk2G8aoxCVgKAsBBG+R4Wy3Wq+hk9IIIH0hMduo7jNa3TzgU2MD8fp+0kK/iR3x5aj0gHw1iZ7NjrdvCCq6wqlTFAWCVOTppE5GIh3qgCVDL0/dTVNnDOlRI+k58s/wBUt4NwsqYkzc+Ui6Et8XjyiWj4amzAFpIBBZQ5NDBw4/YSsosoejaxfmiZLzFAK8xciwIs3nDaHCT/AHMpNvIDsN6D0g4ca4CTa+zkIBjPu1rN+LGlTiBrlA84jpasGWMwvruLaQW4loFpUpUwB1pCh06eIgEacTGBtoCRygzZIaCQbeNqMem21JGGxLUpHjGVt3fRZOImWnOnVIYMbXDCKH41ZbMok6AkuYtTpUtCCiUXQQxe7EB/rASepQWAxsRZjGUWJbmwqx0UrDOIWpCQn6jnwA95gb60jh2gkVNIrtEvmJSo7ggbHaAiuGKeTUImJmiZLCiSFKR3WGhbWLWB4iEUi0v3hmPmoWhRTOsrVtVdWdvSGCpPVgAX9Zy8aUwmGViFvKZdUlG1bZyN78LxHhWmV2MyzKJSoOzhj7RPhKcyXmeQfYxmuEz8ykkmx28IZcRxk5QJamOh8GgPXJRi+scTeItO/nFS+kcJ/FX1IVax5SSL+fKveK44kKKZJOQEgvo7tYwHqK7IntEB8/xB+Wo8YESJhmTUSEkBSlXKnF94eKXh6SkXGYpvcm5bVoK6+UkFJ4xp8iaq4vCMYVtPUkbYMjWdDPMZai1I1biqqlSZaUFwo+UaJwslYp0i+pikcLRLB7INqSDdz9oMYZiCJUtKFEPqfO8DChi1ErMAUnisapxpLJbyvaT6ceFfEYiCjN1I8xEaq4He0UsKrUyJIlTVAkE66FySDeBtcUmQvLqSrKR7eTROcwi31BtDgykiMo07eyk2F7Sftkx3297UaocPRVLVNW/Zp7oa2YjW/IQtYtgkumqkpBJlTO8HNw1iH5PeGzA61ApZISQ2QP4/N7vC7xrXpK5KQxLKfpo0VVobSzsAiQBfU76luj6Otjj30k49VFE1aEqOpcA21MVaTFFpUggsQdtYrYzInJUpSkqKSo94XF7+Lx8wzD1rIJdnsDYwZWyE20EacK+hJ6TwbOFH1hQSkDSTaMtSd3fR/EJE+ckTR31JNxuRfT9bwHRLmKUBkUAFF3tof7esMtJP7OUyW13J84gxWeDKS3x515vAs30hBpwkxEQfM1Bd6bW3hwpm2cAjISfLtHOvNbh6iQkq76WACbgCxb3h5pkLk0+VC3KEmxG7Oz+sK2BMpZUdLC/Pf3+kH5eIy0khSvj08P0YItLiMOXGzChl3ifbhnWMSGlsJhRIAzJJmdbmxvfiKZE0uYDMWASBbwvASZwwhLHMVDOnVvhJZXneGNSoHYxMenmEG4DjyLwkFBK9k8+dS3U7SZVoKsokSAlghIAcBtoTkFIUUpUUpC1eJDsC8GcKkLnU4WVfvCSwJYMC30gtPw2SsALQCRvofUQ0++padhcfkEfIrYLUWoQbKixmYGXzmKQsKqTLq0lNwCXbcXBhrVxJKD96LxwuSFZxLSLMwAAPJxu0IGLTBKmzEszLIPkWHtGqXzshKTEesz3+26sYDo555ZSmI49keNt28jV/TiKGACgY8prQ0Z8jEwpSEqOViEpUNnLX5iCNfMmy0oImOFFn30BidisM9tp2Vpg2HdTuKZXhP8yeRBmfIjjbvoriuOhKwhIzbKa9yNOsZ5UzVBWQy1BTsAxFgYZMHmlM0qAfJdufOJq4on1KZhLJYqPUWDRQwrEQAc791vXuFJYTpp7DpWUAXOR4A034BRokU6E/MUgqPMkO3hHiuqEy5E0tYZve4+sRIxBK0gpNmtAzGMQSJc1Kr5kgADmbfaIoxS1vlIHC2t4PvR3TCS6Tneed6McKEpppYV1bwN4JrmNC/gtYEyJaXuz+8EPxgjfE4sdYUz2+daYdkltJ4Cg3Hp/dIPVvUf0hZwpaUqQVh0vcdNoc8elBdKtbOUspL9NT6PC1OlpNKkBgRfNvreK2HbV1QJ1BIEafnPlzpHEMlx1WzoJ7tKJ//E/i0k0yUoRobtcX0gfNly5C1KnK7yFdn2bF1FJueRQefUQw8OJloKjLPyuepLXMe8dkCfLUlacygDk2Ze14y9imG42gZ1PDf6x41R6PQ1tAOHaROhiDN7EXFhM/tGrMQJC+zdMsl2fZ3D+UB6moHZljqW94mXNspL7t/SOpqSU4KpYNtPRi0H2rTuJPl7Z11WJxTXRzIUkJgaAwezfxojwfKClAr+Bz52HtDtw2iSqbMQqWkt3kkh7PpCpNlFITlYAhgBZukaRhtKiXLQAkOEs+99YNh5U9tTlpzmuKx2MXjH1OKEe2nv27qp49LkJlKJlIzWCGABCtiIoGobUsd4H1M+ZMmMS+QksLX084ZxhkpQT2ktKiAAXuD+d4S6QKsQsFFgLTHG/z3plpt3DD6xY5CfkUEqahIlrUTZLO2/SFSsxlOcuA/wBOkPeJYbnmymACAf3gAF0gOkeD28DGa4zgYNROYhI7RTC+juPYt5QXAutob6siSMzVjo3FNyesHy1P1bg0lcp1OpSUkguwduXKAGKU05EtSkXloIBLh9hp5wblYmhiMwuC3pC7i2LOgyRurMo82AYeohBnEKLqSkwQINRHQlj6kGCciKgkECWQkkE3ZzyvaPiKlJLkAkEMDe/SPmG0EyahM1KTlFiQ3m45QSm8ATEnNLqHN1AFNgrYC+nWClKEuqvu8z7U/wBHY7DJwwCkGdkCALEiZ1iDY9u+uk0oVKUlQuVP4EN9zAgVCQU8wR6CJ6KuMuX2a37RKlZn1za384qT5QE8KT3kkuro7ZhBg4FrI5e1c71pQn6QLz2Aggj5lViZVIX8O7t63itLkWD6KUfq0EE02ZE2YhIHZg/XbyvAeXNsGL626kkx4/RkbT/9H2p/+UMQ+3132yJ0kEmfMzvFTyaJUtBu4KnLHQb/AJecS9sn5QeV1W8t+W5Zt4Gy5hJV9IlKwkgDkYKXJGyketdkz0Qzh9pAEgmQDcDlM635044XxCpQyrIKhof4vLnEtXihWlUuXcqBfp+nhMlzO8DDQeHak5Zkoy++lCi6ug6NpCX8ZsvBw7U6QbTI4SO/uip3SLDWHIUU/Sqbbjb0J7Y0tTVQt2Msp0ZvS31BiwZjwMw+raWmWvurTYg7tuOYPOPfb3eFulFhpYULhQHhb5zqKwC4maurXGbftDlZKm3zpSrzNvs8P4nCEL9okqYqoljKTmQkS2uVF9AObnSCdFuJUufk1a6GIbxMm1j6UBmmwfdvWGmrWFUcpRPeBB9ssJ9WkoUmWsELBDpIII8R42hyo6ALkykqsCwWroNoaew+3EGNmT6eoPIGtv6heSplDaPqUSYi+lzyqtgKymYD4n84hr7TmT/DbwJjuIZCZM1PYlkqDEAm20DRPCFjMbAfQxthzBTtZfn91wy7JUg5g+lMmBYAucVtNVLSlQ+EO4LuBsP6wbqeEhkDTSSCGca+MVeCcZEySvYpmG3Rg3jBusqwW7wAcZvK8V2sJhVXIBm886bab2mwCbdtBKDgqaAZq5oCmJQkAljqHLt/eBNNiK05VEuVG4OzGHmnxIKSC8KFUmWZ+bLmR3zlG+YW9yfSJryUK2SyLzxOaZmiJwzo+lud1vXuN6pcR8RzJiQE91L36vaAsyqKZYSV33HWLdXTa3JDgBJYKF4EIpVFUwFJfNYdOcbqWorUpeZ+edLutPsL2XAUkg9o7JtTRwpiCJZUVksUbdDDRSze1coDi/e2f7xmMmd2ayl9NDGmYGoIlqA0Cn/1AK+8SMa2kDanMkRA3E030c8uC3upUxCjMqcibMSCwVtqUlr+cQYbhq6la1IYMl/F9B4loO8ZKCght0qHnY/WJOABllTFHcpA9CfvDS3lOMh20kXO69/AW40BDQ64s3gfPWglfJVLmBEwFJABbk8MWG8TZ8srISo2zbNz9IMVC0kKCkghWrjXxgAmqQJ65jAd0gbb6+0asY9J6wpsoC50zjvgz2GjfwnEK2k3E3tlaosQwpaZkoy5pIXOQFOLgFWo5w7E3hCn4wCgKSXUlaVAeBeGGTjKFpCgdQ7bjxgHXFtslYsCYPp8/FOB4vqCSZMUYmKDxmfFM5qqYOR+t/vDlMxMO72EIPEK+0nqWxu3tb7RrhMQHFKgVe6FZIdJOWz6j80OrZhzBtAHHi7GLk8EzB4keoitMlu55OPaLiwHfz+hh95v+/A3H0iuDSuEA/M6N8GVR/DzkPoCW8RDtLrdOTRmJqjJKSnRRUkjmC30g5OxCalKVZFhI1JBAIFrc7kROeae67abm9/nM+NUsM+2G4Vp71HxBRoFXNWnWYlJI2cAh/NoK8LcPy5qe0USACRlG4YN4QtoqTNnZmPeSzf+MOnBKiJCnDfvC1m+VMVsAolyFbvG1KK2XHCdJNT4vhkqVLV2acoPxDXUNvGd1WHCWlBv3gH8f7RpeOyyunmpGpSSPEX+0ZtVVilKSght3g+IAS4DFref6761xABTs0LCx2hHU+o1HpeIampGbyMW0UMszJpU7qIy3bQO469YqysJ7SYgZyyiQSwcWd4GlISsJmZy+dtdlhf6naUj+6DtgXMWJi/z4C3DNEahaU7C61D5U/N57DrGqnswAEjKABpsGhF4SqpMgrkIBazzD8ytw/08+cG14qEnX0MLvuobUQrv7dKWxGMHSCgtFkjIeZPPThxmoOKTll5t8zj0P9IBIxxaLKZRzNqxbmesSY9ihmzMoIypSXb+IkD1AHvEeE4ImongKzZSHU2wY+V7B4TS024qFpkETexF5576iP4haXiGzeQOdvgqeox6YkjLLc2ck2DvDxw2CZYmLAzqv4J2Hnr5jlAep4bUsZApOUXQojvA7gttDIiXlDDQBvSKHRzTDUKRnHj+tRvry1POEhzLsqlxVhCaiW+UZ0F0qYO3zAHqPcCEy3ZMxIc6EabW1MaMJlozjjXDJkhaZ8lygllJ1KTz6pPsfGGsShK3AQRMXHd8NNdHltt2VgcCf3bXt51QqQCl+um9/wAmhWxSZmSptUqP1hlkHtHVoMwYHp/V/aBS6VMuZo7kqUXtqS/kIAGVrUhREDf71P6RDRxKk4cWtYXkxeImezOo8GWtCWDjKQX8dYvyqpZBzLJdStTs8SYvKCzLEkpAyHNdIDpuSS7Pt1LCPVFQqJlhvjKsh/ictbzjdKkKtqZF+30BNKKZdQk2MTEwYndz4Z8K9ysUmAEZu7lv4mw9opqqVOEjRy8HKrg2qRLURlUygohKnJF9A125Rbwbg9UzvTnloB+H5lN9B1/vAwvZuBBMyK7fod5nD4EKdVe86ngIzyy01tSoqcxd/E+EXsNlZ1hTswObqA3684k44wpNMsBD5F3S5duYffb1irQlaMwIKTlA7wI1Yj2jxBUnaOZpvpUNYjo1ToyjaHNP7KTzNDMTlIuuWFEiYQ/+UdIfsPkzwhICD+9PcJ3Ye1g99hCdhgeYCod0k+pdveNjwmZ+5lbfu5f+wQX+M3ibLyzgcJFcRgR1aS/nfZjs2pPCY53uKSsUwqflQqanKSWSLG/ra3PlHjDiUSphChZWZumUcod8XpzNkzJbsVJISeRa3vGXT1TJaAn4S6kqB8AFD1eFcfhAhAbH22Hjw4/LVSSWS0vEwAuRrodwOlvO4mitTjh7LNlLWfo/OKuEVSagZpiu67K5sdh5QKq+0EskhQQohLlJAJ6H3i1h+ETEryIvLSQSdAdH1Osa4dhLQWEDUZ3yypdrHqWAiIN5jjbs/Zq5KwRjMKC6EufLMQB4sHgSqeRMyg6BrX3h3p5hRLBWjKVm9gw2S8JmLZZczKUFiSxDi+luYgilqCloItpy9qDjcE22nbSqIOuueUZQeyPH5WXmpBJKVIfXnEuHUWdDuAxI05RYFO8kEDvBKW5sB/WK9NMQkEPud40bQlaQFRrSCnFoWVAmTGt6qzJBRMWhWoJB8RHkg3A1YwR4pklE9StlEEegf3gWucyhcB218YOozsOq7e249e+gqGypSOJq9hBQsykLSFKKrk/K1lerNDqrFgsqQbpIIbYiFTD5yATLKUkKZQUwcHYPyj1SIyzSyyCxGQ380nXyMYcaUlSQDYG+nLsgnvq10dglOpDiSCJhW8fjK/GaLU1OhDJlpToznXzO8X5deqUSOen2gMmqAIj5ilckocm6GNrk30HUw6GSG9pGfzKuj/iAw3s/SeHtTBTYuJiMybDmfdoRMXLTSW3zeL/1EFF1hEjMLE6Dlv8AaKEnD51QVK7NRMtREyzMR3mD3P8AWE33CVDbItA8YrluksP1K+qFyM+d5oZNSxzjVRbwAFxHimnEJBGucD1BEGKvAKlQtJWybNZyXvZ3Z3v4RVFBPo1BUyVotw7FKrGz6P7xqXGypKUqyn8+9INoWDJBiPQ1fXNBWEoYJSwtFJUvvpUxfKof+wMH5uPSJiHShLlwbAFKtweogbQ5EqUoZst7bp1zs+0NYwyIChePMx3AgVvhmg46lE52nnVeTha7qe5BLW5tz5w98N0/ZyR/Eq5PsPLfzhMTUBhl0BL+tvK8PtE3ZS207NH+0RLxH0wofdl3V0mI6LZwqQpE7r+fMzpbcBV9JeJHirLmNEyZkCadTEa0kpJqcM0BeJ1fujs4I9nEEZszSA/EmNiQl0JSuam4CnISSC1gdf1vBkv7aihOn78BQXQAglVI9JSqqc+SX30jMpI1NwHA584B4nVKCly1ApKSUkXDNZvHX3h+wPidc+aVLCAQghJAYk2KrkmwaMwxOeVzVqVqVEnx3ik1IJE3seGsR3eVWP6Ww7SlOOkAxswSLg/Vlu1nfAqVE9QGVzq/s0azwhhkv8PTKIOZIUsF91uk+zekY4Zjt4/aNn4Hn56OQeQy/wDuofaBYxwgIAOvoau/1EdvCjdtDyMd1HisvHEvHxYjyDE9RULGuQA3UM4gwgVAkuB3ZyVH/t/MPpCzx034gbOgOf8AyIEPSlCEzH6SbOqyEIKnCb7BIsbmzOY8hcrzEDjv/MUcNOYtKcPtBIF5OgkE6id8a0vSiqQZEwMQbjqDp73jR8LqyZaCRldItytCBi9KUiTKOZklKQopyuHAPMc4balYU4BKWFm6aQ0/jkYd1KswpIy5fiKlYFgqChlEUdNSICqlSlVrzQCBLzpJLJcEOT1AI1tAJGKKzTAtbZXyt835bWgFVYmtd1uRt+uWloKnEhyFEag37f32VWwnRZxc7JAAN7eA4jnWlq4qps2XtCdnykg+2kL2L1aZk5WRScpy3BF+6H03hKNcWA1uPI+MfE1BcKe4hh54uAJUddP3XQMdCoYVtIJyi8HUXyG6mivxYmYEA2a4aJajD5lQxSQQi9zcHbLs3SIsPEqYjMNT8XQxalV/YFZH8OnNoxi2+qbCwZ8bzu+b6n9INtFhTcRAv2EH58FDaYKASkAkkMbXsknTyMVaPhupylkWctbL7feDWA1IE4k3OU35lxm+8MX47r7xCfxqWDsqTfXPOT8765trB9anany3CsuxWqXNnKYEknT/ACm4bw+0UVh8ozAKdmNtD6RcrkESZFSl3DoX4gun1SW8oE19yFc/eKJKQko4/PGaD0k0prGOpP8AyV3SYrQOD8I7SYJqh3JTAf5lNp5OD6R84tw0p78twygUnlraDXDFUj8JJCVOOzDn/N879czxZqVy5gXLVoRfzG3XQ+kLfzwh8KiwMEcKsdFLODKVC83PH58zrNq2uUClTXNleI1I8dfWJ6GYZgUs7KFvIv8AWKeIky1LlLspBChrdtCPEF4gpsSyyrXClf3jo23EJUAn7ZsdIz8K7JTrQbsRzm0b/nKiuIz3qJUr5ct/FSkg+w940WpmdnLmTE2OZKj6pCj6Rk0xSsxJPylST4WPm4940GqrlGWpJ+eWXHXX+kQ38OcQ/wBYMhf2/wDGTXBJe69x9xPMchIHbA9KMjEATraBvEVWg08wFAX3VEJP8QBy+BeA+DkLkvmUFoJA5M1n6beUBKzE1uvokK8/0IhNMOKX92RBJ1HzdRXHUBoLjMW+c6pBHZhQWcpSxUxHi77t0g3QqSkJVq6lH1FvrAOqqO0IWoAghiNNtC31i9JW8tDBunKLq1gK2k6ZTzBHt2VCTlFUSFIVMS9gTroUnRjtaHDg7FiE/h5qgSkAy1aZkkPl8R9PCAWKoHZIUB3nKT1DP94FS5xyp6aeRcfWNcQiwG+89lu64qgOk8QUbC1bQG/Pvz75rWBOHOPMqfCNS4+sMlXeL3JsyQA50uXi3O4gWhJUlA1Ad3Z+kQ3MNiVKGzpOW6nhi2Ngknsj2purq5MtJWr5RpGbYpULm5lP3lm/gT/b0i1W10yaqYVKcJygDZzcnximm48jFDCMFCdtWZv4Hxt4Dtm4t8OK2RkPeP1UaJK5QK0gpbLY7Nq/Qw5YF+z+RNSmfUhRUtObswcoSVbuLuzW2L+AD8FS/wARUCWsky5aQS4d2YAPveNVUqL2FYTPWaZCe+tsG+8yFFtRTNrVjnGH7PZ0j95TPMlEnMNFptZ9lDrZrQwfsynFNOqQthMlrNuir/V4fysGxjKsRnfhsSnGUClKRmKTuCAVAdL2gePw5UgKRvy4wfnA76df6VfWlKHjKZvv4d2uprSVTIiK4DycWTMSFoIIPLbxG0fFV8c45jwFEEG3CmkNSAQbGic2ZeL1LKcC9oBS6gEs99+kGMNJyv6Rt0RsPYspdTmJHCIPaCJoWJBSiRV6ppkTEFC0BSTqCLfrrCnWYEtNUhMt+zKCXJJAIYXPNy/X1hvSp4+qI1jrnmG3o2xMZb6nBRH2mslx+jEucUAlWUM5Gpyh/d4AKXpDjxeCassHcpPh3Q/0hOxSTl8Ce74P+hCCEgnY0BUO5aq6f+l3ky6ycyQocbQe6BXMx/XrHictk+cSVB+E+UQ4gWQPGBM/eJ0rrEESJo7w1ImzFESmNsxBLWeLmIYbNTlM1gpRUpgXDOAA7bMH6kxN+z9JSFTOYyj1BP2hymIRMDTEJVuHGnhyMDfxv94NT9IO75v9a5Lpohx1TZ+3hYzFuyYJpHwealkpIBUZoD75cqnD+LR2MpWFgS1nLl57uY9V1MZM1MvkpweYItBPA8MNRLMxS8veIT1Aa/q48oFj8OXFJcTcGfQiubwrmwktHMRlyvVDgyWldGpExIUlSlODoQyYFcZCUUoXLAAYaWswYNswDQwcO4HOl0wzKTLdJI+Ygqu5AtZ+e0JmPSVU80S83aJULHQ21/TwVH9p1YWPvyyyBN45d8DdTPTTnXvKcbuJN+23vUGBY4uQSkF0EuR15wxUWMqKlJNgEu/MgMfYCBWHYWkkLUAQxI2vsFDUeBj7QSVKUMoJ1fwO3mWEbHCNBwbaQZuewWmpKcS6kQk8q+jvvOmmxLgbq0AA5DrFKfQLRMCVIUkFGYOkizG99o2BVBLKkqWhBKGKbDun+kVceHaSJyGuqWsD/SWhdOJ2dgqTF4A5HO3lpTycAYUnambzGt+PyKygqeWt/lWfQgOPUQ31NekgEHUOOtiYUZbOUnRTv4kuPPSL9NhylKQiWgl0nKAXJ+V+jmHWllLcp4DupXDYgthYAnaHv71LKmzcmWUVOpQcJDk628IlRh65illCFKcd7KCWDZbt4GPcymmU5PaDKpPeFwQXcBiNQdIP8F1SkzEy2CgpJCjvZL8+cJ4RpKyEG02tnvFYlUbKieW74aU8FwWdMmGSlOYsFPsEq0L+L+hgziOBTadLLSSAr4wDlIO77XLXjSqKklSnKEgEsDzYOw8A59Y9VSQtJSq4UGI6RWPRrZTYmd9Z6q1ZRiShky8r+bQLp6SYQwTmZywBLAty05+cF8TXK7SbLKSlSFKGXNZQzZRcgsdDB6hqCEsLJAYAQo+lKiETkOeVqNgej1YnbJMAR88KASMBmLp1zEWXq3Ni7dDv5CJ6SSxaalkqlAlJ3uNOoJgqjEiFqRt994syMMTPmIdVg5I3IbQHbb0jHVpbKFAGRmNDIg1TxXRYQ2Smxgdut+NJaULK5tu7lBJ2+Jh56x7kyCWFtxqN9If8XwOX2X7sJCk33dQ5E7nxhIqlBJb5iS56Pfy+sJpkbKTp3RlXsH0MjFSSsjkL5kzyiOZmjX7MykTJwcPkTl/lzKBY73AeHtU20Zvw/VJkzgWyguD0fV/MOYcplcNjFVrEoDIvr5kmgudHOYUhCr8Rl+xrV/t4TOKpKTVFZDHsx3tm3fwgxMxEDeFHH8W7ZkpU4UplJG6QxA8HYmM/ykkpQLkn5fnFKYpsBF6X11oTmTIdKU6qc5i7sR0/pBNE1cynJK1OFMbl2fTz0gXTUs0EZ0KBIGqTfn42hrlykok5EA3N31eJ76x9KhBOnj4WonReCcxG0kEpFgc5uCct/OpKaeqShSkg5Sog82TYfeGfhbFkT8xQ7ZUtY6ORvrCwilX2ZKrEK0O8X8OxRSEkoKQPhYiwbwj0oL6HyDbceBGut949rLnRhS3sNKnfO+bXH5p3Kmjpsyzwlp4gqFEshBA6fd4+4lji5lMtKRkWrunkB8xfwt4kRS/ltgZHhIzj1pF3ozEN3gHgDJ7s+2gGJ1S5qs7i6rkWdIUwbpYQu4rndQbMkDa7Fsxvtq3lBRLpSlJ2DfU/WF7EpyilybBarbMLfUQihdyZvfzPqalsPu4bEJcTZST+CD5VGax0EbgP6XiabUZylIDlTMBcl9GioZTgAs7MbXNjv4w2cKTJUuZIzAA95OZg+ndc+0ec+kFaeJHj7d1dIP6iUpSSEwDbPI3prwSjMmShB1Av4m5g1KVAqZWB4mFaANfCOdU6CVHWfWvOkqMnM0vcVTP+KTfZMO0oiWlKACrKkB/z6xnGPVWertfIB7AqP1h7pKwTEJWNFB4tuPFLKDG/lp8HbURjZ693n7z85Um4pxSqZJRLkliA6lEOCE2y6897aCFbFsRTMCQRmWn5rgB9bb+cEaikzocEAgB9nH97/oQE/wDiwhDO8xZcqOyRfup2JO52fm0PLa/uBawDlB4buzde9Ty6q42o4aVPw7PaYpzpLUz+UMfCBSqpQk7Kzf6QVfaFVEsBYybO/mD/AGgnw7WiVUylnRwD5gp+8CUCJI3GKG3G2mcprZRp1N4p1Cdetoil4ilR1ihWYzLYrzjIHL7W1iQ84hQSE6Htt6/mumQCk3rMkgKVkPcWhTOxZRGgPIvD9wfLCEqnKN0JMtOu6ipWvRh6wCNKZiu0U3eUVocXS57oPk3hHUmJpkpWhbhKlZif5gB9orO7TLcJ1HsD51zzEB0TRbjCekplFVwjXr3e6P8AUxihwjUtVU9yxCxfnkLedoX+I8X7VSUpNgST9B7P6xeweoKMihqAkwFlwo2CrQ+GUdwrZ1culQ+XFbAmaIgqqsISVKLBIJPgLmE+m4wS3fSpJ8HHtATini3tEmTLcOO8Tbf4W66mLbmKQkGM6JtpjaBmhE/EROX2xGVa1nMNufs4Hk+8NtH8APjGfS1d9KeRc+dz7fSH/A1haSOr+RiayNp0TrVboRY6t1O6D2XB9K+ChUo5ou0c0pPUResBEE8JAKuUVHmBs8qsF/rLGjMma4ha4ko5aEqyh1OV/wBB5QXpJ4YHo/tAWum5lkdPqY5lCgBHGOypLmIXh3AUHWTxg5Uqy6orv+ukXZHaJllQKgnQWLA8vUExJwHgzzZpmAKTLe2zqJCfYE+LQ74glPZKQEgDKbABtLRri3epzE9/zvpzpHEIxmyGlWTNxkZ8bRWZV1VNKGzMCRnO6g2g/KL/AAbhpmzUMlxLWVFR8mHIDp0j7itEns5C0i6kkK6kHXpb6Q6cGUXZSHa6zmP29oKHQpkbjI7s/auebbJfO1wPlFHTJSdUhxpaPH4BBIJSHGkWAp4+KML9WgKk3+a1Q2lDI0Nx6gMyWQixF4XsOwomTNUTd7eUOGeKk1sikgNYx5eJQISNcuzSnGMS4231Y3g1naqgu20eJ1SWLH5S8DsVqghTdT9YroqwXL7AP9YdWFKQFV16WRAVpTJhtOJ+SU7KUrukjodekSY7wSZacxWVS05S6QLK3zA/K+453g3wHhBb8RMDOGlg7D+Lz2htnoC0KSoOFAgjoQxh3D4QdXKs9OHz5vriulVYdWM2okCNq+e8dgrHhgQTmKpoCW7rByrRQHIe8eaTA5hSJjjKQlQ+48fKCNZUKTnlmwvYDlb8494bWgS5f+UAF9wR9YSDvVonu5wf1up13oRgkIAMZ556dmcUMrK6bLYuSb68rke0eV1s05ZpXcgMlrJcO3U9YuYkoKlLdQKmsw1v/eKNagJloHn9I91LWzZN790n0ionSAewa+r2yb2PCPeap0c8icC+pIPV9Y0DD6rLKQOSRCDIkO0xwAlaU+JLMPqfLqIvJx4SwEN8IaMYxpx1kJRvnwNI4VwNrJVuojw/hKqmYUk5ZaEgltz8oPT8oXOJpS5E6amYGIJI6pOhEazh2HpkBQTookjw2ELH7UcK7aQJqR35Zv1SdR63hgK20AHh5Vk4eM86y3Cakla35P7uPyglKDloH4JS3WTYkEAEXsx9IKy1JBGYhIcc/tDKokUs8Pqq3XVs8ul2GUpKru4PixcbkRbkSQFNcgDupJsC5u25f0ihOnOsjZZUpNwe7sS3X7wUkJGbNuUgfU/eFHUpDn027uVGLii0ATvqzVVRStI/6Y+rfaK2JpC0eI/rEWML78s80ke5/OOzfu9dn9QHHlG+JWTsKGh9PehMqhUml8zQC5Dk7Qw4bMzhBOpB9jABVCSXzZToBd9C2mkFMGnrsJmqVEPzDWvvpGrjYAGtx51qDrVmmxLNmSwIdgfE/lEeKSEqKVJAzCyuoNn8R9PCIewSlhLLuQed3FgWD7bbx8qZjKUf1pGxeBUUm4gju/deEgVXk/Mrq369R7wfwKu7NWQm7e+vr+cUqNKJSqfM5H+IoNuzpHlaK2LYnKmzCqUFJyqGcFtTcEEba+kFYaE7RP1DT5zo+HxK8O51iORGhGvzfTNU48BYmKVdjryVgH4u6DyfU+QvAGcnMb3LWh5wUo/CyxlS2QAhhchwXgmL6ShuAnWDeuiZ6US6CUIgjfevtBiCcoQFOyfW0QzJve8h9TAGmqUomz8qRlUru80gbAbB4+Tq8h1EaD6PEIMOIMkWOvG01NxD4Wskm+tO/DcoJRNV/Gv2H9SYt1kzunwP0hJwviNfZBNiQpjsz6e0RzeJlqISEsAplEn0a19Yz0hhXnFlQTYgX5fqt8LimkthM5etTzZrppkcsxPqw+/pGiU/wgDlGQV1QUmxvYDoAXjVsIqhMlIWNCkRlY2UNpGVz2n8ULBr2lrJzhI7AIq6FxIFvFZRj6FQkpZTVApqUmK0/fwMe1TIimKsfCEnFha0gaGtgmKxnGwtc5QylgogWPM+UfcLpwblQLP3X06mC+IAGYojRRJH8wJBHq4gHUKyrSoWc6+Go8CD7R2ASUiNI+efjSmI6fxTqOrT9OkjP8efGtu4aqM9LJU98gHp3ftBAG0JXCeJpTTAE3llbjzJEG6biCUqWF5gkHUEsx3eHGXElIBzj0FJJVIHEUh8XzQipmJZu8o+OYBUAl4hlQA+3sI9cR4omprZi0fAkDKeYCWfz+jR8qcNRlSq7sCbhutoQOHQSdwM99WU9PIQEqUDZOzpmLnUWrxSTlzCSfhYgRexGekrEouDkBB2uSWI10AvEvYpDBIDFA02JN/CKSCoTzNmIISs9xw2ZKbW6aeseQAt1aVWge58qiY/Fqxa+sNvQceM/ItU2IjskU8v/qIKv5lEk+gYeUUmlKuvX9fd4sYtNEzKX0IX6QOMxoGhcJtx9KUcF62+q/OA3E3/AC87/tq+kdHQJn7e6qr/ANx+aVjNAf8AiE+Q8uXhE9V8afFP2jo6KLn2o5VNX/t5U48XSUpnS2SB8egA2itL28T9BHR0SBkn5qaI96+gqnjv/wBfgr6iPVL8B/l+5jo6HVfaeVJjOhw1TEiFG9/kH0VH2OgZ+0fNBWyc6nptUeA+0WsVQOyklg5SXLa9/fnHR0ajXkf/AGTWyKqcQ/4h/kV/tVC7T6L/AJvsqPkdDZzVWE/ZTAr4h4/eGbCf+WT4q+sfY6EcR9h5jypzBfar5voDU/4iv5j9Y+1v+GrwP3j5HQZf+BHb5mhOf5Fch6UPwT4l+Kf90WE6q/m/KOjoZc/xdhrQf7uXqKixb4x4Ro3Af/L/APkY6OhBX+nH/bTOD/1Hf6UwLjo6OiOvM1cqGI5+h8DHR0SWP8qedEOVZlUfAn+dX2gPjv8A+z9DHR0fQ2/tHIVyZ/ynsorQKNr8/wDaYgxs/uj4n/bHR0LJ+08/Q1lP3jtoLhvxK/k/KGSr/wANPgPoI6OgzevzfWF/YnmaN8DJCqmY4fuI1vtDD+0OUn8NM7osUtYWuNOUdHRHxP8A+g3/ANXvT+H/ANIrkfKstl/N5fQxBM1jo6HKRczr/9k="
                                           class="table-image-wrapper position-relative"
                                           data-fancybox="table-images<?= $i; ?>">

                                            <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTExMWFhUXGBoaGBgYGBcaHRobHxodGhgeIB0dHSggGhsnHR8dITEhJSkrLi4uHR8zODMtNygtLisBCgoKDg0OGxAQGy0mICYyNzMvLS0tLS0tLzIwLS8vLy8tLS0tLS0vLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIALIBHAMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAFBgMEBwACAf/EAEEQAAECBAQDBgQDBgQGAwAAAAECEQADBCEFEjFBBlFhEyJxgZGhMkKxwRTR8AcjUnLh8TNikrIkNGNzgqIVQ8L/xAAbAQADAAMBAQAAAAAAAAAAAAADBAUBAgYAB//EADoRAAEDAgMECQQBAwMFAQAAAAECAxEAIQQxQRJRYXEFE4GRobHB0fAiMuHxFAYjMzRCUnKCorLCNf/aAAwDAQACEQMRAD8AdMGrlVKQClr+rRJVT5K5mScGSkHXR4lTQJpcvZkpS7XvrFDGcJVNSsJIJBCgT7xz7aEK2VITdJII36jwvaKvh1lT8TsJOWcideyqNMEonESpjyzcB7CO45l5US82j6+UDsIw4hZEwkJCtebQX4gqkqypXlVLCv7QZYQopUlN7zwp7+QgYtHVq24GmZt4mhOCFK1KEtF1JZn94P0WD/hpebObB1DaKHD2RKiZSCq7eHKHSkJyOtLHcRsGlOkBNgJkxroONriOe6g9JY1SFbMGDFrSfOKGYPicuYLK02ifFKvIjOi7aiAFZLH4qYuUxGUWFr7x4o8QJQVKScr97pAFYXbWWwoRrntWvI4GhKw6FLCkawdk53Ex6TV6jxBQStS0nnYEgcrxXqOJGBZJeL+HYpKSky1EXNhzB0gBxbSSpcsGVse9d4wvCNqSklXZ6VthVNOPhDyNknLd8taixcSjMUntCb2LtBPD6xMyUglri8L/AAFMK5UxKg6X7pPuImmTJRK5ctTKcsBsd4DiEO2KIk8xbKDu0yrL7Y21tZ7JmRlBG68VJiAkks9wbHlFrCMilKck5bB9IQqlM6WVdo4IOp3HOGbglRmdoCe6wMFU0Sn6e7lr7/inMRhAjDFxK5G+iM1MmVOVMBLqYHlBpCwUukWPKBnZypg7MiynBME0y0SJQSn4Ui93gKmlIaDqjEX5gxEDhFS31AhIuVcdRp7UHn4r2IXnDcvCB1TjYGUJBUV+sXaxKZ57gCibP03gvTUUpCR3EuGu0eZQCjaV2Tl2edYafbaMFBMZyY+XqliE7spaVy0KKmFgDePeF4smcgKPdUCxB5wTpqnMcrQMVhS1Kmp7iQVOCznSNQ2pSJ0mMojh+fOvIU0UlDliLzOk5RrRZM4HlFCul5iACGfQwNrKKamXMaa60hwBvFPh9c7uqmkd4sEq1aNg26NnriABJFyfIdhFbIw6dgrSsWtGu+3zhUVJh6fxBMtBWymJfug7w0VyghBUpI7oj1S0YlqUUn4tthAziKqQuTNQSXYi0DfXtwkHl81j0rLjvXupiSLDWdK8cNY6qpBGVm5aQfzpd4TOEqSd2B7EoSHIu79YLVc5aUsZS7aq5/nDqMGYUpRj/jJsd0ajjRcVh2ziFJaIF4gG450TxNKZiCBrsRq8DKionSZAUUhTDvXNhFnD0Mys3dZ784rYtVlY7IAHO/h5wopl1MFYBndl6Gl8OpJUEC6Zkz43q1hVaFyUqKWe7RdQhK090AeUAsMJdlkMNhE9ZiolMGPSNcS2cOoASoRuvesvNfWQnW+dXjg6FAKKUhSTYsIkJD5Slm94rYbigUDm7pGx5RJWAzE5kqsm569Iy2es2WU5m8Z8zQyFpVsuGw8KgxJE0yz2OUEaPCjUYzOMx5jGYgMyd3/tFzE62atKxIzHre3MCBfBq0Z1GeSFKNn+8MM4fqUuIJkTETnYXO4DzFWcLh0BhTiglUZACVaeFNGK0FWZIWgiwzEb6QKoptWECyi99OcPCMQSEsSIoVmKICm6RspjDtNJ2jPOCfeKjpxC3B1fVi2okW4xnQ7GMSzSwnKSTe12A3gTSYq4y5xoXO8MWLyhLkT+yQXWlgEhze0ZbMWUKSC4JNwbNGOrSUpSDJi/Pu+TVLA4RGMw5Qq0HQ8JNtBpWgUlFNnSitKXGx0e+0TysEExCUTRlWk5ibc/eKvCGNJRIKFKFlFvDf3jzPxkqmEgskkB+kbuqRsJWidoaTuqM6DhMWUgxBgHy8LUVpsLRJeYFG6rizchBVc1xfQxUoqXtQoZiEwExSqmyFlJuzMeY2PSF3y+tIeSmxF7gXnPtrDrqQ4etVflwsK+Vk1EusS9kqR3m6EtFbi3GqfJ2SFELa2QBr8+cDcLwqdUTJk1SibsCQwPQHpFCdh8uTUBU0kqQQWDEEbQzhVqaQEnOBf0nviqGHdbfxTewlSglGYt9QOvDIfIq9RVMsLQpd2DAmIK45lq/hUfKJJ1TTzVPlygkMeXO0WcQw9qXtpJKy7kN8oGsbFhRUTM/PzSaujnkvIGIMIUrOcpm05DdnHKjvD89JkCQgMRqRzMWqzhtJUlaDkKQ1t/6wl4BXzJYJY97aHeXjaCAXvCmIxOyqVcgR5UZZTh8QpGHXI1vPA8DxqnS4J2wV2wcA5b3doIYlRy5NOsSEBBYFkjW8U8MxjMVpQknvE+sSnEFJnErSQMu43hrCPpU4ptdpFyRvGk7xQS6459YVlcDQwQIilidioSlBe4U7ed/aC9LUTahKmDJ5GzwNxelQoFcxKQXcNzMXsKxtKBlUwADDrC+JbKU7CgbGfg5VvicShK0tK+68mRBm8dhoRh+Krk1LAZQ+XL5wVrsfX2hAS5cBhzgbW4d20xVRLSop2OxVppBGlwqbIImzU7ZrXazX6x5QLrO1BIFznFGfx2FceSEoM7IHCQfEETfjUVfxWpDFADtd9ovYJxKZoJUO9m25NAbE8AEwLUlbTDcDYveL/DGBJQhLqJU/eP26RjEGGQDInKL6TR3FYT+KNlH1mMwe2+WRt3UbWFzlOhJ0vA3Fqaaupp0oCR2Zcg7wzyVZbAW6RQxRaZaxNIuH+kEbc61Ab2juMgZQZgxuHfekA84lY6sCwMa5iD3ivXbnP2YHe5QLxOjPaBKkkJLknn0irRY/LVVZ7gKGW8HarEJazlKg3jGE4ZDUkqJO1AFhYXGVYebeQsJUCi0zrx130o0qZkmcLlKM4BY2h6XPSQzg9IVMcxSn70pKFFT6vYdY+4dXpEsqLZto1xgIjq7pNuXHuprGqcWlLjjeyALG17WkaTodaJcRYQlFMtSVqzAZtfNoEcFrKpE4qLsrunkGizV4pMnAykJzFQb84puqSRKUoS3S2UbjnD7IQfqaT9MeQ3mgYbEKVh14aRY7UnheB3X3TUq6uWpRKFZdidv7x6p62WqaAsgkaHrEWO9nKpVIQQokgvbeElNQykh77xjEH+QFIIggm4MxaKt4XCN45raBMTbLTWtHxKnRN+fKSHcdNoGTcQXKlqEtRHI9d4PYXhwMmUZku+UEP1F3i2nB6dQOZAPRy0KYdtS1hsWULzlutGdc2+44hWcpScsx8tSNwpjMxfaoWSCxYsRe7wLVRrM4hiA7lR5PGkow6UiWooSNSfSF2nIUuYtVyLJB0EEfbU2qRmq3AfM6osdLL69RaQE7WW4QKt0hkqyJQvNMca8t4JTcJzF1G8B52JIkjvoAKi4YR7wybUzkZ0pLOQLjaNSkI2kKSZB0sIikXkP4ZQWSYVNxwphEpSg4PIwncYcHzZ60zZYbu98ZgHINi3Noe5NkgcgB4sI9KXzhNLLbSusQTMG827QZojGKdZVtIj53VnHD+DrKFDs1FjyNjy8YnqcPmpYiWoJFjY2h3NSlG4HT7wv1fFKXVL+I3DD2h9tCC2layQTuj5FDx74xRVtJSDMzrYZcalwSsWiSSW1s5a2kUJ0tVXVKlpIsACdgNSfeAtRiKnAUe65YQZ4GnpE6YrQlFutw8bNL63ZbVlz3etIs4Z19lTiR9KMydcrDvo1xHiaKVKZKbfu3BA0YsPW/pGcVxExfaZi5JJEHP2hVbz0ga9mw9VGFZC8qbm+/3gmI++R2cuWvzlXbdD4JDWHS6n7lC/jVuXTPvc3T1a8M6qr8PJMoJOcoy62c3gVLmI/DSSpu07UhJ3KMofxDwyUeDpqJOcqVnu3Lu6eNoFh1KUdlGhI8Yt2VP/AKiU6WAE5BRnxA+bzwv5wTAcqc81lBgQAem8M6sKlGT2YSEkhwRqCYGSAUyAFatcdIqTOJkJbWw+0Dw2IbbSdtJMm9ptl6Vz/UICEpTn45e5qDh6rRJnTJS7KKmBPMbQU4jmgylNrZvWBlLSyJijULBUpZzAPYCzW948SqZM1yVqASQQAbPGuJacSErEQTJzn4edUcVh0IT/AGUkECDuB555+NWpfDSAxmrUsPe+h2bpERopBZkaHV+sT4Xiip0wSVBu9cjprBrEaNHZqKEAKFw2p5iGXkKdlSNN8ybXg5W4xSOGfQpX9y5MCc++arYdLCMqZaT2Y3OgOpi7istS5a2/hNtz4R4owUoD+cWjMgTWI6kqRaJmBlxjhw96ypICwU6ZdlZ7MMzMknuse8Dq+hHlDPhykyHQtYdbKHQ7x4x6lCpaikAFzNV1IT/aM6mVanzEk2G8aoxCVgKAsBBG+R4Wy3Wq+hk9IIIH0hMduo7jNa3TzgU2MD8fp+0kK/iR3x5aj0gHw1iZ7NjrdvCCq6wqlTFAWCVOTppE5GIh3qgCVDL0/dTVNnDOlRI+k58s/wBUt4NwsqYkzc+Ui6Et8XjyiWj4amzAFpIBBZQ5NDBw4/YSsosoejaxfmiZLzFAK8xciwIs3nDaHCT/AHMpNvIDsN6D0g4ca4CTa+zkIBjPu1rN+LGlTiBrlA84jpasGWMwvruLaQW4loFpUpUwB1pCh06eIgEacTGBtoCRygzZIaCQbeNqMem21JGGxLUpHjGVt3fRZOImWnOnVIYMbXDCKH41ZbMok6AkuYtTpUtCCiUXQQxe7EB/rASepQWAxsRZjGUWJbmwqx0UrDOIWpCQn6jnwA95gb60jh2gkVNIrtEvmJSo7ggbHaAiuGKeTUImJmiZLCiSFKR3WGhbWLWB4iEUi0v3hmPmoWhRTOsrVtVdWdvSGCpPVgAX9Zy8aUwmGViFvKZdUlG1bZyN78LxHhWmV2MyzKJSoOzhj7RPhKcyXmeQfYxmuEz8ykkmx28IZcRxk5QJamOh8GgPXJRi+scTeItO/nFS+kcJ/FX1IVax5SSL+fKveK44kKKZJOQEgvo7tYwHqK7IntEB8/xB+Wo8YESJhmTUSEkBSlXKnF94eKXh6SkXGYpvcm5bVoK6+UkFJ4xp8iaq4vCMYVtPUkbYMjWdDPMZai1I1biqqlSZaUFwo+UaJwslYp0i+pikcLRLB7INqSDdz9oMYZiCJUtKFEPqfO8DChi1ErMAUnisapxpLJbyvaT6ceFfEYiCjN1I8xEaq4He0UsKrUyJIlTVAkE66FySDeBtcUmQvLqSrKR7eTROcwi31BtDgykiMo07eyk2F7Sftkx3297UaocPRVLVNW/Zp7oa2YjW/IQtYtgkumqkpBJlTO8HNw1iH5PeGzA61ApZISQ2QP4/N7vC7xrXpK5KQxLKfpo0VVobSzsAiQBfU76luj6Otjj30k49VFE1aEqOpcA21MVaTFFpUggsQdtYrYzInJUpSkqKSo94XF7+Lx8wzD1rIJdnsDYwZWyE20EacK+hJ6TwbOFH1hQSkDSTaMtSd3fR/EJE+ckTR31JNxuRfT9bwHRLmKUBkUAFF3tof7esMtJP7OUyW13J84gxWeDKS3x515vAs30hBpwkxEQfM1Bd6bW3hwpm2cAjISfLtHOvNbh6iQkq76WACbgCxb3h5pkLk0+VC3KEmxG7Oz+sK2BMpZUdLC/Pf3+kH5eIy0khSvj08P0YItLiMOXGzChl3ifbhnWMSGlsJhRIAzJJmdbmxvfiKZE0uYDMWASBbwvASZwwhLHMVDOnVvhJZXneGNSoHYxMenmEG4DjyLwkFBK9k8+dS3U7SZVoKsokSAlghIAcBtoTkFIUUpUUpC1eJDsC8GcKkLnU4WVfvCSwJYMC30gtPw2SsALQCRvofUQ0++padhcfkEfIrYLUWoQbKixmYGXzmKQsKqTLq0lNwCXbcXBhrVxJKD96LxwuSFZxLSLMwAAPJxu0IGLTBKmzEszLIPkWHtGqXzshKTEesz3+26sYDo555ZSmI49keNt28jV/TiKGACgY8prQ0Z8jEwpSEqOViEpUNnLX5iCNfMmy0oImOFFn30BidisM9tp2Vpg2HdTuKZXhP8yeRBmfIjjbvoriuOhKwhIzbKa9yNOsZ5UzVBWQy1BTsAxFgYZMHmlM0qAfJdufOJq4on1KZhLJYqPUWDRQwrEQAc791vXuFJYTpp7DpWUAXOR4A034BRokU6E/MUgqPMkO3hHiuqEy5E0tYZve4+sRIxBK0gpNmtAzGMQSJc1Kr5kgADmbfaIoxS1vlIHC2t4PvR3TCS6Tneed6McKEpppYV1bwN4JrmNC/gtYEyJaXuz+8EPxgjfE4sdYUz2+daYdkltJ4Cg3Hp/dIPVvUf0hZwpaUqQVh0vcdNoc8elBdKtbOUspL9NT6PC1OlpNKkBgRfNvreK2HbV1QJ1BIEafnPlzpHEMlx1WzoJ7tKJ//E/i0k0yUoRobtcX0gfNly5C1KnK7yFdn2bF1FJueRQefUQw8OJloKjLPyuepLXMe8dkCfLUlacygDk2Ze14y9imG42gZ1PDf6x41R6PQ1tAOHaROhiDN7EXFhM/tGrMQJC+zdMsl2fZ3D+UB6moHZljqW94mXNspL7t/SOpqSU4KpYNtPRi0H2rTuJPl7Z11WJxTXRzIUkJgaAwezfxojwfKClAr+Bz52HtDtw2iSqbMQqWkt3kkh7PpCpNlFITlYAhgBZukaRhtKiXLQAkOEs+99YNh5U9tTlpzmuKx2MXjH1OKEe2nv27qp49LkJlKJlIzWCGABCtiIoGobUsd4H1M+ZMmMS+QksLX084ZxhkpQT2ktKiAAXuD+d4S6QKsQsFFgLTHG/z3plpt3DD6xY5CfkUEqahIlrUTZLO2/SFSsxlOcuA/wBOkPeJYbnmymACAf3gAF0gOkeD28DGa4zgYNROYhI7RTC+juPYt5QXAutob6siSMzVjo3FNyesHy1P1bg0lcp1OpSUkguwduXKAGKU05EtSkXloIBLh9hp5wblYmhiMwuC3pC7i2LOgyRurMo82AYeohBnEKLqSkwQINRHQlj6kGCciKgkECWQkkE3ZzyvaPiKlJLkAkEMDe/SPmG0EyahM1KTlFiQ3m45QSm8ATEnNLqHN1AFNgrYC+nWClKEuqvu8z7U/wBHY7DJwwCkGdkCALEiZ1iDY9u+uk0oVKUlQuVP4EN9zAgVCQU8wR6CJ6KuMuX2a37RKlZn1za384qT5QE8KT3kkuro7ZhBg4FrI5e1c71pQn6QLz2Aggj5lViZVIX8O7t63itLkWD6KUfq0EE02ZE2YhIHZg/XbyvAeXNsGL626kkx4/RkbT/9H2p/+UMQ+3132yJ0kEmfMzvFTyaJUtBu4KnLHQb/AJecS9sn5QeV1W8t+W5Zt4Gy5hJV9IlKwkgDkYKXJGyketdkz0Qzh9pAEgmQDcDlM635044XxCpQyrIKhof4vLnEtXihWlUuXcqBfp+nhMlzO8DDQeHak5Zkoy++lCi6ug6NpCX8ZsvBw7U6QbTI4SO/uip3SLDWHIUU/Sqbbjb0J7Y0tTVQt2Msp0ZvS31BiwZjwMw+raWmWvurTYg7tuOYPOPfb3eFulFhpYULhQHhb5zqKwC4maurXGbftDlZKm3zpSrzNvs8P4nCEL9okqYqoljKTmQkS2uVF9AObnSCdFuJUufk1a6GIbxMm1j6UBmmwfdvWGmrWFUcpRPeBB9ssJ9WkoUmWsELBDpIII8R42hyo6ALkykqsCwWroNoaew+3EGNmT6eoPIGtv6heSplDaPqUSYi+lzyqtgKymYD4n84hr7TmT/DbwJjuIZCZM1PYlkqDEAm20DRPCFjMbAfQxthzBTtZfn91wy7JUg5g+lMmBYAucVtNVLSlQ+EO4LuBsP6wbqeEhkDTSSCGca+MVeCcZEySvYpmG3Rg3jBusqwW7wAcZvK8V2sJhVXIBm886bab2mwCbdtBKDgqaAZq5oCmJQkAljqHLt/eBNNiK05VEuVG4OzGHmnxIKSC8KFUmWZ+bLmR3zlG+YW9yfSJryUK2SyLzxOaZmiJwzo+lud1vXuN6pcR8RzJiQE91L36vaAsyqKZYSV33HWLdXTa3JDgBJYKF4EIpVFUwFJfNYdOcbqWorUpeZ+edLutPsL2XAUkg9o7JtTRwpiCJZUVksUbdDDRSze1coDi/e2f7xmMmd2ayl9NDGmYGoIlqA0Cn/1AK+8SMa2kDanMkRA3E030c8uC3upUxCjMqcibMSCwVtqUlr+cQYbhq6la1IYMl/F9B4loO8ZKCght0qHnY/WJOABllTFHcpA9CfvDS3lOMh20kXO69/AW40BDQ64s3gfPWglfJVLmBEwFJABbk8MWG8TZ8srISo2zbNz9IMVC0kKCkghWrjXxgAmqQJ65jAd0gbb6+0asY9J6wpsoC50zjvgz2GjfwnEK2k3E3tlaosQwpaZkoy5pIXOQFOLgFWo5w7E3hCn4wCgKSXUlaVAeBeGGTjKFpCgdQ7bjxgHXFtslYsCYPp8/FOB4vqCSZMUYmKDxmfFM5qqYOR+t/vDlMxMO72EIPEK+0nqWxu3tb7RrhMQHFKgVe6FZIdJOWz6j80OrZhzBtAHHi7GLk8EzB4keoitMlu55OPaLiwHfz+hh95v+/A3H0iuDSuEA/M6N8GVR/DzkPoCW8RDtLrdOTRmJqjJKSnRRUkjmC30g5OxCalKVZFhI1JBAIFrc7kROeae67abm9/nM+NUsM+2G4Vp71HxBRoFXNWnWYlJI2cAh/NoK8LcPy5qe0USACRlG4YN4QtoqTNnZmPeSzf+MOnBKiJCnDfvC1m+VMVsAolyFbvG1KK2XHCdJNT4vhkqVLV2acoPxDXUNvGd1WHCWlBv3gH8f7RpeOyyunmpGpSSPEX+0ZtVVilKSght3g+IAS4DFref6761xABTs0LCx2hHU+o1HpeIampGbyMW0UMszJpU7qIy3bQO469YqysJ7SYgZyyiQSwcWd4GlISsJmZy+dtdlhf6naUj+6DtgXMWJi/z4C3DNEahaU7C61D5U/N57DrGqnswAEjKABpsGhF4SqpMgrkIBazzD8ytw/08+cG14qEnX0MLvuobUQrv7dKWxGMHSCgtFkjIeZPPThxmoOKTll5t8zj0P9IBIxxaLKZRzNqxbmesSY9ihmzMoIypSXb+IkD1AHvEeE4ImongKzZSHU2wY+V7B4TS024qFpkETexF5576iP4haXiGzeQOdvgqeox6YkjLLc2ck2DvDxw2CZYmLAzqv4J2Hnr5jlAep4bUsZApOUXQojvA7gttDIiXlDDQBvSKHRzTDUKRnHj+tRvry1POEhzLsqlxVhCaiW+UZ0F0qYO3zAHqPcCEy3ZMxIc6EabW1MaMJlozjjXDJkhaZ8lygllJ1KTz6pPsfGGsShK3AQRMXHd8NNdHltt2VgcCf3bXt51QqQCl+um9/wAmhWxSZmSptUqP1hlkHtHVoMwYHp/V/aBS6VMuZo7kqUXtqS/kIAGVrUhREDf71P6RDRxKk4cWtYXkxeImezOo8GWtCWDjKQX8dYvyqpZBzLJdStTs8SYvKCzLEkpAyHNdIDpuSS7Pt1LCPVFQqJlhvjKsh/ictbzjdKkKtqZF+30BNKKZdQk2MTEwYndz4Z8K9ysUmAEZu7lv4mw9opqqVOEjRy8HKrg2qRLURlUygohKnJF9A125Rbwbg9UzvTnloB+H5lN9B1/vAwvZuBBMyK7fod5nD4EKdVe86ngIzyy01tSoqcxd/E+EXsNlZ1hTswObqA3684k44wpNMsBD5F3S5duYffb1irQlaMwIKTlA7wI1Yj2jxBUnaOZpvpUNYjo1ToyjaHNP7KTzNDMTlIuuWFEiYQ/+UdIfsPkzwhICD+9PcJ3Ye1g99hCdhgeYCod0k+pdveNjwmZ+5lbfu5f+wQX+M3ibLyzgcJFcRgR1aS/nfZjs2pPCY53uKSsUwqflQqanKSWSLG/ra3PlHjDiUSphChZWZumUcod8XpzNkzJbsVJISeRa3vGXT1TJaAn4S6kqB8AFD1eFcfhAhAbH22Hjw4/LVSSWS0vEwAuRrodwOlvO4mitTjh7LNlLWfo/OKuEVSagZpiu67K5sdh5QKq+0EskhQQohLlJAJ6H3i1h+ETEryIvLSQSdAdH1Osa4dhLQWEDUZ3yypdrHqWAiIN5jjbs/Zq5KwRjMKC6EufLMQB4sHgSqeRMyg6BrX3h3p5hRLBWjKVm9gw2S8JmLZZczKUFiSxDi+luYgilqCloItpy9qDjcE22nbSqIOuueUZQeyPH5WXmpBJKVIfXnEuHUWdDuAxI05RYFO8kEDvBKW5sB/WK9NMQkEPud40bQlaQFRrSCnFoWVAmTGt6qzJBRMWhWoJB8RHkg3A1YwR4pklE9StlEEegf3gWucyhcB218YOozsOq7e249e+gqGypSOJq9hBQsykLSFKKrk/K1lerNDqrFgsqQbpIIbYiFTD5yATLKUkKZQUwcHYPyj1SIyzSyyCxGQ380nXyMYcaUlSQDYG+nLsgnvq10dglOpDiSCJhW8fjK/GaLU1OhDJlpToznXzO8X5deqUSOen2gMmqAIj5ilckocm6GNrk30HUw6GSG9pGfzKuj/iAw3s/SeHtTBTYuJiMybDmfdoRMXLTSW3zeL/1EFF1hEjMLE6Dlv8AaKEnD51QVK7NRMtREyzMR3mD3P8AWE33CVDbItA8YrluksP1K+qFyM+d5oZNSxzjVRbwAFxHimnEJBGucD1BEGKvAKlQtJWybNZyXvZ3Z3v4RVFBPo1BUyVotw7FKrGz6P7xqXGypKUqyn8+9INoWDJBiPQ1fXNBWEoYJSwtFJUvvpUxfKof+wMH5uPSJiHShLlwbAFKtweogbQ5EqUoZst7bp1zs+0NYwyIChePMx3AgVvhmg46lE52nnVeTha7qe5BLW5tz5w98N0/ZyR/Eq5PsPLfzhMTUBhl0BL+tvK8PtE3ZS207NH+0RLxH0wofdl3V0mI6LZwqQpE7r+fMzpbcBV9JeJHirLmNEyZkCadTEa0kpJqcM0BeJ1fujs4I9nEEZszSA/EmNiQl0JSuam4CnISSC1gdf1vBkv7aihOn78BQXQAglVI9JSqqc+SX30jMpI1NwHA584B4nVKCly1ApKSUkXDNZvHX3h+wPidc+aVLCAQghJAYk2KrkmwaMwxOeVzVqVqVEnx3ik1IJE3seGsR3eVWP6Ww7SlOOkAxswSLg/Vlu1nfAqVE9QGVzq/s0azwhhkv8PTKIOZIUsF91uk+zekY4Zjt4/aNn4Hn56OQeQy/wDuofaBYxwgIAOvoau/1EdvCjdtDyMd1HisvHEvHxYjyDE9RULGuQA3UM4gwgVAkuB3ZyVH/t/MPpCzx034gbOgOf8AyIEPSlCEzH6SbOqyEIKnCb7BIsbmzOY8hcrzEDjv/MUcNOYtKcPtBIF5OgkE6id8a0vSiqQZEwMQbjqDp73jR8LqyZaCRldItytCBi9KUiTKOZklKQopyuHAPMc4balYU4BKWFm6aQ0/jkYd1KswpIy5fiKlYFgqChlEUdNSICqlSlVrzQCBLzpJLJcEOT1AI1tAJGKKzTAtbZXyt835bWgFVYmtd1uRt+uWloKnEhyFEag37f32VWwnRZxc7JAAN7eA4jnWlq4qps2XtCdnykg+2kL2L1aZk5WRScpy3BF+6H03hKNcWA1uPI+MfE1BcKe4hh54uAJUddP3XQMdCoYVtIJyi8HUXyG6mivxYmYEA2a4aJajD5lQxSQQi9zcHbLs3SIsPEqYjMNT8XQxalV/YFZH8OnNoxi2+qbCwZ8bzu+b6n9INtFhTcRAv2EH58FDaYKASkAkkMbXsknTyMVaPhupylkWctbL7feDWA1IE4k3OU35lxm+8MX47r7xCfxqWDsqTfXPOT8765trB9anany3CsuxWqXNnKYEknT/ACm4bw+0UVh8ozAKdmNtD6RcrkESZFSl3DoX4gun1SW8oE19yFc/eKJKQko4/PGaD0k0prGOpP8AyV3SYrQOD8I7SYJqh3JTAf5lNp5OD6R84tw0p78twygUnlraDXDFUj8JJCVOOzDn/N879czxZqVy5gXLVoRfzG3XQ+kLfzwh8KiwMEcKsdFLODKVC83PH58zrNq2uUClTXNleI1I8dfWJ6GYZgUs7KFvIv8AWKeIky1LlLspBChrdtCPEF4gpsSyyrXClf3jo23EJUAn7ZsdIz8K7JTrQbsRzm0b/nKiuIz3qJUr5ct/FSkg+w940WpmdnLmTE2OZKj6pCj6Rk0xSsxJPylST4WPm4940GqrlGWpJ+eWXHXX+kQ38OcQ/wBYMhf2/wDGTXBJe69x9xPMchIHbA9KMjEATraBvEVWg08wFAX3VEJP8QBy+BeA+DkLkvmUFoJA5M1n6beUBKzE1uvokK8/0IhNMOKX92RBJ1HzdRXHUBoLjMW+c6pBHZhQWcpSxUxHi77t0g3QqSkJVq6lH1FvrAOqqO0IWoAghiNNtC31i9JW8tDBunKLq1gK2k6ZTzBHt2VCTlFUSFIVMS9gTroUnRjtaHDg7FiE/h5qgSkAy1aZkkPl8R9PCAWKoHZIUB3nKT1DP94FS5xyp6aeRcfWNcQiwG+89lu64qgOk8QUbC1bQG/Pvz75rWBOHOPMqfCNS4+sMlXeL3JsyQA50uXi3O4gWhJUlA1Ad3Z+kQ3MNiVKGzpOW6nhi2Ngknsj2purq5MtJWr5RpGbYpULm5lP3lm/gT/b0i1W10yaqYVKcJygDZzcnximm48jFDCMFCdtWZv4Hxt4Dtm4t8OK2RkPeP1UaJK5QK0gpbLY7Nq/Qw5YF+z+RNSmfUhRUtObswcoSVbuLuzW2L+AD8FS/wARUCWsky5aQS4d2YAPveNVUqL2FYTPWaZCe+tsG+8yFFtRTNrVjnGH7PZ0j95TPMlEnMNFptZ9lDrZrQwfsynFNOqQthMlrNuir/V4fysGxjKsRnfhsSnGUClKRmKTuCAVAdL2gePw5UgKRvy4wfnA76df6VfWlKHjKZvv4d2uprSVTIiK4DycWTMSFoIIPLbxG0fFV8c45jwFEEG3CmkNSAQbGic2ZeL1LKcC9oBS6gEs99+kGMNJyv6Rt0RsPYspdTmJHCIPaCJoWJBSiRV6ppkTEFC0BSTqCLfrrCnWYEtNUhMt+zKCXJJAIYXPNy/X1hvSp4+qI1jrnmG3o2xMZb6nBRH2mslx+jEucUAlWUM5Gpyh/d4AKXpDjxeCassHcpPh3Q/0hOxSTl8Ce74P+hCCEgnY0BUO5aq6f+l3ky6ycyQocbQe6BXMx/XrHictk+cSVB+E+UQ4gWQPGBM/eJ0rrEESJo7w1ImzFESmNsxBLWeLmIYbNTlM1gpRUpgXDOAA7bMH6kxN+z9JSFTOYyj1BP2hymIRMDTEJVuHGnhyMDfxv94NT9IO75v9a5Lpohx1TZ+3hYzFuyYJpHwealkpIBUZoD75cqnD+LR2MpWFgS1nLl57uY9V1MZM1MvkpweYItBPA8MNRLMxS8veIT1Aa/q48oFj8OXFJcTcGfQiubwrmwktHMRlyvVDgyWldGpExIUlSlODoQyYFcZCUUoXLAAYaWswYNswDQwcO4HOl0wzKTLdJI+Ygqu5AtZ+e0JmPSVU80S83aJULHQ21/TwVH9p1YWPvyyyBN45d8DdTPTTnXvKcbuJN+23vUGBY4uQSkF0EuR15wxUWMqKlJNgEu/MgMfYCBWHYWkkLUAQxI2vsFDUeBj7QSVKUMoJ1fwO3mWEbHCNBwbaQZuewWmpKcS6kQk8q+jvvOmmxLgbq0AA5DrFKfQLRMCVIUkFGYOkizG99o2BVBLKkqWhBKGKbDun+kVceHaSJyGuqWsD/SWhdOJ2dgqTF4A5HO3lpTycAYUnambzGt+PyKygqeWt/lWfQgOPUQ31NekgEHUOOtiYUZbOUnRTv4kuPPSL9NhylKQiWgl0nKAXJ+V+jmHWllLcp4DupXDYgthYAnaHv71LKmzcmWUVOpQcJDk628IlRh65illCFKcd7KCWDZbt4GPcymmU5PaDKpPeFwQXcBiNQdIP8F1SkzEy2CgpJCjvZL8+cJ4RpKyEG02tnvFYlUbKieW74aU8FwWdMmGSlOYsFPsEq0L+L+hgziOBTadLLSSAr4wDlIO77XLXjSqKklSnKEgEsDzYOw8A59Y9VSQtJSq4UGI6RWPRrZTYmd9Z6q1ZRiShky8r+bQLp6SYQwTmZywBLAty05+cF8TXK7SbLKSlSFKGXNZQzZRcgsdDB6hqCEsLJAYAQo+lKiETkOeVqNgej1YnbJMAR88KASMBmLp1zEWXq3Ni7dDv5CJ6SSxaalkqlAlJ3uNOoJgqjEiFqRt994syMMTPmIdVg5I3IbQHbb0jHVpbKFAGRmNDIg1TxXRYQ2Smxgdut+NJaULK5tu7lBJ2+Jh56x7kyCWFtxqN9If8XwOX2X7sJCk33dQ5E7nxhIqlBJb5iS56Pfy+sJpkbKTp3RlXsH0MjFSSsjkL5kzyiOZmjX7MykTJwcPkTl/lzKBY73AeHtU20Zvw/VJkzgWyguD0fV/MOYcplcNjFVrEoDIvr5kmgudHOYUhCr8Rl+xrV/t4TOKpKTVFZDHsx3tm3fwgxMxEDeFHH8W7ZkpU4UplJG6QxA8HYmM/ykkpQLkn5fnFKYpsBF6X11oTmTIdKU6qc5i7sR0/pBNE1cynJK1OFMbl2fTz0gXTUs0EZ0KBIGqTfn42hrlykok5EA3N31eJ76x9KhBOnj4WonReCcxG0kEpFgc5uCct/OpKaeqShSkg5Sog82TYfeGfhbFkT8xQ7ZUtY6ORvrCwilX2ZKrEK0O8X8OxRSEkoKQPhYiwbwj0oL6HyDbceBGut949rLnRhS3sNKnfO+bXH5p3Kmjpsyzwlp4gqFEshBA6fd4+4lji5lMtKRkWrunkB8xfwt4kRS/ltgZHhIzj1pF3ozEN3gHgDJ7s+2gGJ1S5qs7i6rkWdIUwbpYQu4rndQbMkDa7Fsxvtq3lBRLpSlJ2DfU/WF7EpyilybBarbMLfUQihdyZvfzPqalsPu4bEJcTZST+CD5VGax0EbgP6XiabUZylIDlTMBcl9GioZTgAs7MbXNjv4w2cKTJUuZIzAA95OZg+ndc+0ec+kFaeJHj7d1dIP6iUpSSEwDbPI3prwSjMmShB1Av4m5g1KVAqZWB4mFaANfCOdU6CVHWfWvOkqMnM0vcVTP+KTfZMO0oiWlKACrKkB/z6xnGPVWertfIB7AqP1h7pKwTEJWNFB4tuPFLKDG/lp8HbURjZ693n7z85Um4pxSqZJRLkliA6lEOCE2y6897aCFbFsRTMCQRmWn5rgB9bb+cEaikzocEAgB9nH97/oQE/wDiwhDO8xZcqOyRfup2JO52fm0PLa/uBawDlB4buzde9Ty6q42o4aVPw7PaYpzpLUz+UMfCBSqpQk7Kzf6QVfaFVEsBYybO/mD/AGgnw7WiVUylnRwD5gp+8CUCJI3GKG3G2mcprZRp1N4p1Cdetoil4ilR1ihWYzLYrzjIHL7W1iQ84hQSE6Htt6/mumQCk3rMkgKVkPcWhTOxZRGgPIvD9wfLCEqnKN0JMtOu6ipWvRh6wCNKZiu0U3eUVocXS57oPk3hHUmJpkpWhbhKlZif5gB9orO7TLcJ1HsD51zzEB0TRbjCekplFVwjXr3e6P8AUxihwjUtVU9yxCxfnkLedoX+I8X7VSUpNgST9B7P6xeweoKMihqAkwFlwo2CrQ+GUdwrZ1culQ+XFbAmaIgqqsISVKLBIJPgLmE+m4wS3fSpJ8HHtATini3tEmTLcOO8Tbf4W66mLbmKQkGM6JtpjaBmhE/EROX2xGVa1nMNufs4Hk+8NtH8APjGfS1d9KeRc+dz7fSH/A1haSOr+RiayNp0TrVboRY6t1O6D2XB9K+ChUo5ou0c0pPUResBEE8JAKuUVHmBs8qsF/rLGjMma4ha4ko5aEqyh1OV/wBB5QXpJ4YHo/tAWum5lkdPqY5lCgBHGOypLmIXh3AUHWTxg5Uqy6orv+ukXZHaJllQKgnQWLA8vUExJwHgzzZpmAKTLe2zqJCfYE+LQ74glPZKQEgDKbABtLRri3epzE9/zvpzpHEIxmyGlWTNxkZ8bRWZV1VNKGzMCRnO6g2g/KL/AAbhpmzUMlxLWVFR8mHIDp0j7itEns5C0i6kkK6kHXpb6Q6cGUXZSHa6zmP29oKHQpkbjI7s/auebbJfO1wPlFHTJSdUhxpaPH4BBIJSHGkWAp4+KML9WgKk3+a1Q2lDI0Nx6gMyWQixF4XsOwomTNUTd7eUOGeKk1sikgNYx5eJQISNcuzSnGMS4231Y3g1naqgu20eJ1SWLH5S8DsVqghTdT9YroqwXL7AP9YdWFKQFV16WRAVpTJhtOJ+SU7KUrukjodekSY7wSZacxWVS05S6QLK3zA/K+453g3wHhBb8RMDOGlg7D+Lz2htnoC0KSoOFAgjoQxh3D4QdXKs9OHz5vriulVYdWM2okCNq+e8dgrHhgQTmKpoCW7rByrRQHIe8eaTA5hSJjjKQlQ+48fKCNZUKTnlmwvYDlb8494bWgS5f+UAF9wR9YSDvVonu5wf1up13oRgkIAMZ556dmcUMrK6bLYuSb68rke0eV1s05ZpXcgMlrJcO3U9YuYkoKlLdQKmsw1v/eKNagJloHn9I91LWzZN790n0ionSAewa+r2yb2PCPeap0c8icC+pIPV9Y0DD6rLKQOSRCDIkO0xwAlaU+JLMPqfLqIvJx4SwEN8IaMYxpx1kJRvnwNI4VwNrJVuojw/hKqmYUk5ZaEgltz8oPT8oXOJpS5E6amYGIJI6pOhEazh2HpkBQTookjw2ELH7UcK7aQJqR35Zv1SdR63hgK20AHh5Vk4eM86y3Cakla35P7uPyglKDloH4JS3WTYkEAEXsx9IKy1JBGYhIcc/tDKokUs8Pqq3XVs8ul2GUpKru4PixcbkRbkSQFNcgDupJsC5u25f0ihOnOsjZZUpNwe7sS3X7wUkJGbNuUgfU/eFHUpDn027uVGLii0ATvqzVVRStI/6Y+rfaK2JpC0eI/rEWML78s80ke5/OOzfu9dn9QHHlG+JWTsKGh9PehMqhUml8zQC5Dk7Qw4bMzhBOpB9jABVCSXzZToBd9C2mkFMGnrsJmqVEPzDWvvpGrjYAGtx51qDrVmmxLNmSwIdgfE/lEeKSEqKVJAzCyuoNn8R9PCIewSlhLLuQed3FgWD7bbx8qZjKUf1pGxeBUUm4gju/deEgVXk/Mrq369R7wfwKu7NWQm7e+vr+cUqNKJSqfM5H+IoNuzpHlaK2LYnKmzCqUFJyqGcFtTcEEba+kFYaE7RP1DT5zo+HxK8O51iORGhGvzfTNU48BYmKVdjryVgH4u6DyfU+QvAGcnMb3LWh5wUo/CyxlS2QAhhchwXgmL6ShuAnWDeuiZ6US6CUIgjfevtBiCcoQFOyfW0QzJve8h9TAGmqUomz8qRlUru80gbAbB4+Tq8h1EaD6PEIMOIMkWOvG01NxD4Wskm+tO/DcoJRNV/Gv2H9SYt1kzunwP0hJwviNfZBNiQpjsz6e0RzeJlqISEsAplEn0a19Yz0hhXnFlQTYgX5fqt8LimkthM5etTzZrppkcsxPqw+/pGiU/wgDlGQV1QUmxvYDoAXjVsIqhMlIWNCkRlY2UNpGVz2n8ULBr2lrJzhI7AIq6FxIFvFZRj6FQkpZTVApqUmK0/fwMe1TIimKsfCEnFha0gaGtgmKxnGwtc5QylgogWPM+UfcLpwblQLP3X06mC+IAGYojRRJH8wJBHq4gHUKyrSoWc6+Go8CD7R2ASUiNI+efjSmI6fxTqOrT9OkjP8efGtu4aqM9LJU98gHp3ftBAG0JXCeJpTTAE3llbjzJEG6biCUqWF5gkHUEsx3eHGXElIBzj0FJJVIHEUh8XzQipmJZu8o+OYBUAl4hlQA+3sI9cR4omprZi0fAkDKeYCWfz+jR8qcNRlSq7sCbhutoQOHQSdwM99WU9PIQEqUDZOzpmLnUWrxSTlzCSfhYgRexGekrEouDkBB2uSWI10AvEvYpDBIDFA02JN/CKSCoTzNmIISs9xw2ZKbW6aeseQAt1aVWge58qiY/Fqxa+sNvQceM/ItU2IjskU8v/qIKv5lEk+gYeUUmlKuvX9fd4sYtNEzKX0IX6QOMxoGhcJtx9KUcF62+q/OA3E3/AC87/tq+kdHQJn7e6qr/ANx+aVjNAf8AiE+Q8uXhE9V8afFP2jo6KLn2o5VNX/t5U48XSUpnS2SB8egA2itL28T9BHR0SBkn5qaI96+gqnjv/wBfgr6iPVL8B/l+5jo6HVfaeVJjOhw1TEiFG9/kH0VH2OgZ+0fNBWyc6nptUeA+0WsVQOyklg5SXLa9/fnHR0ajXkf/AGTWyKqcQ/4h/kV/tVC7T6L/AJvsqPkdDZzVWE/ZTAr4h4/eGbCf+WT4q+sfY6EcR9h5jypzBfar5voDU/4iv5j9Y+1v+GrwP3j5HQZf+BHb5mhOf5Fch6UPwT4l+Kf90WE6q/m/KOjoZc/xdhrQf7uXqKixb4x4Ro3Af/L/APkY6OhBX+nH/bTOD/1Hf6UwLjo6OiOvM1cqGI5+h8DHR0SWP8qedEOVZlUfAn+dX2gPjv8A+z9DHR0fQ2/tHIVyZ/ynsorQKNr8/wDaYgxs/uj4n/bHR0LJ+08/Q1lP3jtoLhvxK/k/KGSr/wANPgPoI6OgzevzfWF/YnmaN8DJCqmY4fuI1vtDD+0OUn8NM7osUtYWuNOUdHRHxP8A+g3/ANXvT+H/ANIrkfKstl/N5fQxBM1jo6HKRczr/9k="
                                                 alt="">

                                            <div class="black-line-more-pic position-absolute">Skatīt galeriju</div>
                                        </a>
                                        <div class="text-left">Abelia mosanensis - Mosanas abēlija</div>
                                    </div>
                                </td>

                                <td>

                                    <?php if ($i == 2) echo "<span class=\"new\">Jaunums</span>" ?>

                                    <div>Pa-s</div>
                                </td>

                                <td>
                                    <div class="margin-table <?php if ($i == 5 || $i == 13) echo " less-than-10 " ?>">
                                        20-40
                                    </div>
                                    <div class="margin-table <?php if ($i > 5 && $i < 13) echo " amount-10-30 " ?>">
                                        40-60
                                    </div>
                                </td>

                                <td>
                                    <div class="margin-table  <?php if ($i == 5 || $i == 13) echo " less-than-10 " ?>">
                                        C2
                                    </div>
                                    <div class="margin-table  <?php if ($i > 5 && $i < 13) echo " amount-10-30 " ?>">
                                        C7,5
                                    </div>
                                </td>

                                <td>
                                    <div class="margin-table <?php if ($i == 5 || $i == 13) echo " less-than-10 " ?>">
                                        5.00
                                    </div>
                                    <div class="margin-table  <?php if ($i > 5 && $i < 13) echo " amount-10-30 " ?>">
                                        8.50
                                    </div>
                                </td>

                                <td>
                                    <div class="margin-table-inputs d-flex search-window-wrapper <?php if ($i == 5 || $i == 13) echo " less-than-10 " ?>">
                                        <input type="text" name="from">
                                        <a type="submit" class="search-button">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                                 viewBox="0 0 20 20">
                                                <path d="M15.5,20A1.5,1.5,0,1,1,17,18.5,1.5,1.5,0,0,1,15.5,20Zm-9,0A1.5,1.5,0,1,1,8,18.5,1.5,1.5,0,0,1,6.5,20Zm8.9-5H6.716A3.015,3.015,0,0,1,3.783,12.63l-.032-.174L2.142,2H1A1,1,0,0,1,.884.006L1,0H3a1,1,0,0,1,.966.738l.023.11L4.473,4H19a.988.988,0,0,1,.742.329A1,1,0,0,1,20,5.1l-.019.117L18.324,12.65a3.014,3.014,0,0,1-2.746,2.344ZM4.781,6l.947,6.152a1,1,0,0,0,.876.841L6.716,13H15.4a1,1,0,0,0,.945-.671l.031-.112L17.753,6Z"/>
                                            </svg>
                                        </a>
                                    </div>
                                    <div class="margin-table-inputs d-flex search-window-wrapper  <?php if ($i > 5 && $i < 13) echo " amount-10-30 " ?>">
                                        <input type="text" name="till">
                                        <a type="submit" class="search-button">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                                 viewBox="0 0 20 20">
                                                <path d="M15.5,20A1.5,1.5,0,1,1,17,18.5,1.5,1.5,0,0,1,15.5,20Zm-9,0A1.5,1.5,0,1,1,8,18.5,1.5,1.5,0,0,1,6.5,20Zm8.9-5H6.716A3.015,3.015,0,0,1,3.783,12.63l-.032-.174L2.142,2H1A1,1,0,0,1,.884.006L1,0H3a1,1,0,0,1,.966.738l.023.11L4.473,4H19a.988.988,0,0,1,.742.329A1,1,0,0,1,20,5.1l-.019.117L18.324,12.65a3.014,3.014,0,0,1-2.746,2.344ZM4.781,6l.947,6.152a1,1,0,0,0,.876.841L6.716,13H15.4a1,1,0,0,0,.945-.671l.031-.112L17.753,6Z"/>
                                            </svg>
                                        </a>
                                    </div>
                                </td>
                                <?php } ?>
                            </tr>

                        </table>


                    </div>

                    <div class="d-flex justify-content-center w-100 pagination-wrapper">
                        <nav aria-label="Page navigation">
                            <ul class="pagination">
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" width="8"
                                                                      height="14" viewBox="0 0 8 14"><path
                                                        d="M6.293,13.707l-6-6a1,1,0,0,1,0-1.414l6-6A1,1,0,0,1,7.707,1.707L2.414,7l5.293,5.293a1,1,0,1,1-1.414,1.414Z"
                                                        fill="#fff"/></svg></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" width="8"
                                                                      height="14" viewBox="0 0 8 14"><path
                                                        d="M1.707,13.707l6-6a1,1,0,0,0,0-1.414l-6-6A1,1,0,1,0,.293,1.707L5.586,7,.293,12.293a1,1,0,1,0,1.414,1.414Z"
                                                        fill="#fff"/></svg></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>

            <div class="extra-information">
                <div class="d-flex w-100 title-descriptions">
                    <img src="images/table-icons/star.svg" alt="" class="star-icon">Informācija
                </div>

                <div class="d-flex table-description-wrapper">
                    <div class="d-flex flex-column w-100 margin-r-20">
                        <div class="single-element-table-description">
                            <div class="new-icon sourceSansPro-semibold red-color">jaunums</div>
                            - sortimenta papildinājums
                        </div>

                        <div class="single-element-table-description">
                            <div class="new-icon green-color">***</div>
                            - pieejams vasaras otrā pusē
                        </div>

                        <div class="single-element-table-description">
                            <div class="new-icon less-than-10"></div>
                            – augu skaits ierobežotā daudzumā, mazāk par 10 gab.
                        </div>

                        <div class="single-element-table-description">
                            <div class="new-icon amount-10-30"></div>
                            – ierobežotā daudzumā, no 10 līdz 30 gab.
                        </div>

                        <div class="single-element-table-description">
                            <div class="new-icon bold text-uppercase">SK</div>
                            – sakņu kamols auduma maisā vai tīklā ietītas auga saknes
                        </div>

                        <div class="single-element-table-description">
                            <div class="new-icon bold">(+) bambuss</div>
                            – stādam piestiprināts atbalsts
                        </div>

                        <div class="single-element-table-description">
                            <div class="new-icon bold text-uppercase">S.A.</div>
                            – stumbra apkārtmērs 1 m augstumā, cm
                        </div>
                    </div>

                    <div class="d-flex flex-column w-100">
                        <div class="single-element-table-description">
                            <div class="new-icon food-icon"><img src="images/table-icons/edams.png" alt=""></div>
                            – ēdams
                        </div>

                        <div class="single-element-table-description">
                            <div class="new-icon bold text-uppercase">Pa</div>
                            – potējuma augstums nokarenām formām vai augstumbrā potēta koka augstums, cm
                        </div>

                        <div class="single-element-table-description">
                            <div class="new-icon bold text-uppercase">Pa-s</div>
                            - potēts uz sakņu kakla, augstums cm
                        </div>

                        <div class="single-element-table-description">
                            <div class="new-icon bold text-uppercase">C2</div>
                            - 2 litra konteiners
                        </div>

                        <div class="single-element-table-description">
                            <div class="new-icon bold text-uppercase">150+</div>
                            - lielāks par 150 cm
                        </div>
                    </div>

                </div>


            </div>


        </div>


    </div>

<?php require_once "footer.php"; ?>