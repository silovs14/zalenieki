<div class="order-success-message">
    <div class="d-flex flex-column align-items-center text-center">
        <div class="green-rotate-icon-wrapper">
            <div class="green-rotating-icon">
                <img src="images/icons/tree-success.svg" alt="">
            </div>
        </div>
        <h1>Paldies par pasūtījumu</h1>
        <p>Nam pulvinar facilisis lacus. Ut sit amet ultrices sapien. Morbi sed sollicitudin mauris.</p>
        <a href="#" class="green-button button prevent-shaking-animation">Uz sākumlapu</a>
    </div>
</div>