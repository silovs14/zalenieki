<a class="border-for-slick">
    <div class="single-slider-element">
        <img src="images/slider-landing/slider-landing-<?= $image;?>.png" alt="">
        <div class="text-wrapper">
            <p class="date">Sep 02, 2021</p>
            <h2><?=$titles[$title]?></h2>
            <div class="read-more">lasīt vairāk
                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 8 14"><path id="Icon" d="M1.707,13.707l6-6a1,1,0,0,0,0-1.414l-6-6A1,1,0,1,0,.293,1.707L5.586,7,.293,12.293a1,1,0,1,0,1.414,1.414Z" /></svg>
            </div>
        </div>
    </div>
</a>