

<div class="image-modal-window">
    <div class="modal" id="banner" tabindex="-1">
        <div class="modal-dialog d-inline-block w-100">
            <div class="modal-content d-block">
                <div class="align-modal-helper d-flex flex-wrap align-items-center justify-content-center position-relative banner-img">
                        <img src="images/banner.jpg"
                             class="slide-image">
                        <button class="modal-cross position-absolute" data-dismiss="modal">
                            <img src="images/icons/cross.svg">
                        </button>
                        <a href="#" class="position-absolute banner action-button">
                        </a>
                </div>
            </div>
        </div>


    </div>
</div>