jQuery(function ($) {
    let body = $("body");


    $('.selectric').selectric();

    // ACTIVE MENU NAVBAR
    //CLOSE & OPEN MOB MENU
    $(document).keyup(function(e) {
        if (e.keyCode == 27) { // escape key maps to keycode `27`
            e.preventDefault();
            $(".mobile-menu").addClass("mobile-menu-hidden");
            $(".mobile-background").addClass("mobile-background-hidden");
            $("#burger-mob").addClass("collapsed");
            $('body').removeClass('modal-open-special');

        }
    });



    /*MENU HIDE ON SCROLLING navbar hide*/
    let header = $(".navbar-header");
    let lastScroll = $(window).scrollTop();
    (lastScroll == 0) ? header.removeClass('navbar-scroll') : header.addClass('navbar-scroll');
    let currentScroll;
    $(window).on("scroll", function () {
        currentScroll = $(this).scrollTop();
        if (currentScroll > lastScroll + 200) {
            lastScroll = $(this).scrollTop();
            header.fadeOut();
        }
        if (currentScroll < lastScroll) {
            lastScroll = $(this).scrollTop();
            header.fadeIn();
        }
        (currentScroll == 0) ? header.removeClass('navbar-scroll') : header.addClass('navbar-scroll');
    });
    /*END MENU HIDE ON SCROLLING*/

    /*MOBILE MENU*/
    $('body').on("click", ".menu-opener", function (e) {
        e.preventDefault();
        $(".mobile-menu").toggleClass("mobile-menu-hidden");
        $(".mobile-background").toggleClass("mobile-background-hidden");
        $("#burger-mob").toggleClass("collapsed");
        $('body').toggleClass('modal-open-special');
    });
    /*END MOBILE MENU*/

    /*SHOW SLIDE BY PREVIEW CLICK*/
    let imageNumber;
    let slideNumber;
    body.on("click", ".slider-image-link", function () {
        imageNumber = $(this).attr('data-counter');
        $('.image-modal-window').find('.carousel-item').each(function (index) {
            $(this).removeClass('active');
            slideNumber = $(this).attr("data-counter");
            if (slideNumber == imageNumber) {
                $(this).addClass('active');
            }
        });
    });
    /*END SHOW SLIDE BY PREVIEW CLICK*/

    /*CLOSE MODAL FIX*/
    body.on('click', ".image-modal-window .align-modal-helper", function (e) {
        if (e.target !== this) return;
        $(this).parents('.image-modal-window').find('.modal-cross').trigger('click');
    });
    /*CLOSE MODAL FIX*/
    /*HEIGHT SLIDER TRANSITION*/
    body.on('slide.bs.carousel', '.carousel', function (e) {
        let nextH = $(e.relatedTarget).height();
        $(this).find('.active.carousel-item').parent().animate({
            height: nextH
        }, 500);
    });
    $(window).on('hidden.bs.modal', function () {
        $('.carousel .carousel-inner').css("height", "auto");
    });
    $(window).resize(function () {
        $('.carousel .carousel-inner').css("height", "auto");
    });
    /*END HEIGHT SLIDER TRANSITION*/



// input number
    $('.btn-plus, .btn-minus').on('click', function(e) {
        const isNegative = $(e.target).closest('.btn-minus').is('.btn-minus');
        const input = $(e.target).closest('.input-group').find('input');
        if (input.is('input')) {
            input[0][isNegative ? 'stepDown' : 'stepUp']()
        }
    })
// input number




// Vari izmanto šo vai savu formu
//
//     /*SUBSCRIBE FORM*/
//     let preloader = $(".preloader");
//     //name
//     let nameInput = $(".input-name");
//     //phone
//     let phoneInput = $(".input-phone");
//
//
//
//     //email
//     let emailInput = $(".input-email");
//     let  nonValidEmail = $(".non-valid-email-warning");
//     let  emptyFieldsValidationMessage = $(".empty-field-warning");
//     //checkbox
//
//     let privacyInputParent = $(".privacy-child");
//
//     /*CONTACT FORM*/
//     body.on("submit", "#contact-form", function (e) {
//         preloader.show();
//         e.preventDefault(e);
//         let contactFormData = $(this).serialize();
//
//         $.ajax({
//             url: "/wp-admin/admin-ajax.php",
//             type: "post",
//             data: {
//                 action: 'contactFormHandler',
//                 contactFormData: contactFormData,
//             },
//             dataType: 'json',
//             success: function (response) {
//
//                 let contactFormChecker = 0;
//
//                 if (response["form-name"] == false) {
//                     nameInput.parent().addClass("warning-validation");
//                     contactFormChecker++;
//                 } else {
//                     nameInput.parent().removeClass("warning-validation");
//                 }
//                 if (response["form-email"] == false) {
//                     emailInput.parent().addClass("warning-validation");
//                     nonValidEmail.addClass("warning-message-visible");
//                     contactFormChecker++;
//                 } else {
//                     emailInput.parent().removeClass("warning-validation");
//                     nonValidEmail.removeClass("warning-message-visible");
//                 }
//                 if (response["form-phone"] == false) {
//                     phoneInput.parent().addClass("warning-validation");
//                     contactFormChecker++;
//                 } else {
//                     phoneInput.parent().removeClass("warning-validation");
//                 }
//
//                 if ( (response["form-email"] == false) || (response["form-name"] == false) || (response["form-phone"] == false)|| (response["form-privacy"] == false) ){
//                     emptyFieldsValidationMessage.addClass("warning-message-visible");
//                     contactFormChecker++
//                 } else {
//                     emptyFieldsValidationMessage.removeClass("warning-message-visible");
//                 }
//                 if (response["form-privacy"] == false) {
//                     privacyInputParent.addClass("warning-validation");
//                     contactFormChecker++
//                 } else {
//                     privacyInputParent.removeClass("warning-validation");
//                 }
//                 preloader.hide();
//                 if (contactFormChecker == 0) {
//                     $("#contact-form").remove();
//                     $(".success-submit-wrapper").addClass("show-success-message");
//                 }
//             }
//         });
//
//     });
//     /*END CONTACT FORM*/

});  // jQuery(function ($) END
