jQuery(function ($) {
    $('.multiple-items').slick({
        infinite: true,

        slidesToShow: 3,

        autoplaySpeed: 2000,
        dots: false,
        prevArrow:"<div type='button' class='slick-prev pull-left'><i class='' aria-hidden='true'> <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"8\" height=\"14\" viewBox=\"0 0 8 14\"><path id=\"Icon\" d=\"M6.293,13.707l-6-6a1,1,0,0,1,0-1.414l6-6A1,1,0,0,1,7.707,1.707L2.414,7l5.293,5.293a1,1,0,1,1-1.414,1.414Z\" fill=\"#fff\"/></svg></i></div>",
        nextArrow:"<div type='button' class='slick-next pull-right'><i class='' aria-hidden='true'> <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"8\" height=\"14\" viewBox=\"0 0 8 14\"><path id=\"Icon\" d=\"M1.707,13.707l6-6a1,1,0,0,0,0-1.414l-6-6A1,1,0,1,0,.293,1.707L5.586,7,.293,12.293a1,1,0,1,0,1.414,1.414Z\" fill=\"#fff\"/></svg></i></div>",
        responsive: [
            {
                breakpoint: 900,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false,

                }
            },
            {
                breakpoint: 766,
                settings: {
                    slidesToShow: 2.2,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false,
                    arrows: false,

                }
            },
                 {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1.07,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: false,
                        arrows: false,

                    }
                },



            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
});  // jQuery(function ($) END

