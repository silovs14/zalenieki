<div class="hero-wrapper <?php  if (!strpos($_SERVER['REQUEST_URI'],"index.php")) echo " page-content "; ?> ">
    <div class="semiopacity-main-wrapper position-relative">


        <div class="semiopacity-image position-relative">
            <img class="absolute-cover-img" src="images/hero-bg.png" alt="">
            <div class="semiopacity-layer w-100 h-100 position-absolute"></div>
            <div class="content-wrapper">

            <div class="top-semiopacity text-center">
                <?php  if (strpos($_SERVER['REQUEST_URI'],"index.php")) : ?>
                        <h1 class="main-title main-title-large hero-title">Zaļenieku kokaudzētavas <br> stādu sortiments</h1>
                        <p>Vairāk nekā 1000 sugu un šķirņu <br> stādu Jūsu dārzam</p>

                <?php endif;?>
                <?php  if (strpos($_SERVER['REQUEST_URI'],"news.php" && !"single-news.php")) : ?>

                        <h1 class="main-title main-title-large hero-title">Jaunumi</h1>

                <?php endif;?>
                <?php  if (strpos($_SERVER['REQUEST_URI'],"single-news.php")) : ?>

                        <h1 class="main-title main-title-large hero-title">Mūsu kokaudzētava</h1>

                <?php endif;?>
                <?php  if (strpos($_SERVER['REQUEST_URI'],"partners.php")) : ?>

                        <h1 class="main-title main-title-large hero-title">Sadarbības partneri</h1>

                <?php endif;?>
                 <?php  if (strpos($_SERVER['REQUEST_URI'],"contacts.php")) : ?>

                        <h1 class="main-title main-title-large hero-title">Kontakti</h1>

                <?php endif;?>
                <?php  if (strpos($_SERVER['REQUEST_URI'],"gallery.php")) : ?>

                        <h1 class="main-title main-title-large hero-title">Galerija</h1>

                <?php endif;?>
                <?php  if (strpos($_SERVER['REQUEST_URI'],"about-us.php")) : ?>

                        <h1 class="main-title main-title-large hero-title">Par Mums</h1>

                <?php endif;?>
                <?php  if (strpos($_SERVER['REQUEST_URI'],"assortment.php")) : ?>


                        <h1 class="main-title main-title-large hero-title">Lapu koki un krūmi</h1>
                        <p>Sortiments</p>

                <?php endif;?>
                <?php  if (strpos($_SERVER['REQUEST_URI'],"basket.php")) : ?>


                        <h1 class="main-title main-title-large hero-title">Grozs</h1>

                <?php endif;?>
            </div>



            </div>
        </div>
        <?php  if (strpos($_SERVER['REQUEST_URI'],"index.php")) : ?>
            <div class="fly-over-wrapper content-wrapper d-flex" style="background-image: url('./images/bg/hero-green-bg.svg');">

                <div class="d-flex flex-column align-items-center single-side w-100">
                    <img src="images/icons/skuju-koki.svg" alt="" class="hero-tree-image">
                    <h2>Skuju koki</h2>
                    <a href="#" class="button prevent-shaking-animation text-capitalize">APSKATĪT SORTIMENTU</a>
                </div>


                <div class="d-flex flex-column align-items-center single-side w-100">
                    <img src="images/icons/lapu-koki.svg" alt="" class="hero-tree-image">
                    <h2>Lapu koki</h2>
                    <a href="#" class="button prevent-shaking-animation text-capitalize">APSKATĪT SORTIMENTU</a>
                </div>

            </div>
        <?php endif;?>


    </div>

</div>
