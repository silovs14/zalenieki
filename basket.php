<?php require_once "header.php"; ?>

    <div class="basket default-page-min-height">

        <!-- HERO  -->
        <?php require "included/inc_hero.php"; ?>
        <!-- HERO END -->


        <div class="content-wrapper">
            <h2 class="main-title-h2">Pirkumu grozs</h2>

            <div class="table-wrapper-basket wysiwyg-style">
                <div class="wysiwyg-table-wrapper">
                    <table class="basket-table " style="width:100%">
                        <tr class="text-center">
                            <th>Nosaukums</th>
                            <th>Daudzums</th>
                            <th>Izmēri, cm (augstums, platums)</th>
                            <th>Kont.liel (litri)</th>
                            <th>Cena ar PVN (EUR)</th>
                            <th>Summa</th>

                        </tr>

                        <?php
                        $count = 1 + 3;
                        for ($i = 1;
                        $i < $count;
                        $i++) : ?>

                        <tr>
                            <td>

                                <div class="d-flex align-items-center">
                                    <div>
                                        <a href="" class="delete-item-from-basket icon-shake"><img
                                                    src="images/icons/delete-icon.svg" alt=""></a>
                                    </div>
                                    <a href="https://media.istockphoto.com/photos/green-leaves-background-picture-id140476290?b=1&k=20&m=140476290&s=170667a&w=0&h=Yh5NX36LSL44qKxYh_9e0MmVCL1ZxqGbp0jdc-k4gwI="
                                       class="table-image-wrapper position-relative"
                                       data-fancybox="table-images-basket<?= $i; ?>">

                                        <img src="https://media.istockphoto.com/photos/green-leaves-background-picture-id140476290?b=1&k=20&m=140476290&s=170667a&w=0&h=Yh5NX36LSL44qKxYh_9e0MmVCL1ZxqGbp0jdc-k4gwI="
                                             alt="" class="basket-table-image">
                                    </a>
                                    <div class="text-left">Abelia mosanensis - Mosanas abēlija</div>
                                </div>

                            </td>

                            <td class="number-input">
                                <div class="input-group inline-group">
                                    <div class="input-group-prepend">
                                        <button class="btn-minus">
                                            -
                                        </button>
                                    </div>
                                    <input class="form-control quantity" min="0" name="quantity" type="number">
                                    <div class="input-group-append">
                                        <button class=" btn-plus">
                                            +
                                        </button>
                                    </div>
                                </div>
                            </td>

                            <td>
                                20-40
                            </td>

                            <td>
                                C7,5
                            </td>

                            <td>
                                5.00
                            </td>

                            <td class="green-color sourceSansPro-semibold">
                                5.00 €
                            </td>

                            <?php endfor; ?>
                        </tr>

                    </table>


                </div>
                <div class="grey-total-container d-flex justify-content-end">
                    <div>Kopējais augu skaits:&nbsp;<span>3</span></div>
                    <div>Summa bez PVN:&nbsp;<span>13.20 €</span></div>
                    <div>PVN:&nbsp;<span>1.80 €</span></div>
                    <div>Kopējā summa:&nbsp;<span>15.00 €</span></div>
                </div>
            </div>
        </div>


        <div style="background-image: url('./images/bg/landing-bg.png');">
            <form action="#" name="contact-form-basket" id="contact-form-basket" novalidate>
                <div class="d-flex spec-order-form-wrapper">
                    <div class="w-100 left-form-part">

                        <div class="form-contacts w-100">
                            <h2>Informācija par pircēju</h2>

                            <div class="single-input">
                                <input placeholder="E-pasta adrese" id="email-contacts" type="text">
                            </div>
                            <div class="d-flex legal-person-radio">

                                <!--Radiobutton -->
                                <div class="checkbox-single w-100">
                                    <div class="d-flex justify-content-between align-items-center w-100">
                                        <div class="privacy-child w-100">
                                            <input type="radio" name="legalStatus" id="private"
                                                   class="position-absolute w-100">
                                            <label for="private"
                                                   class="d-inline-flex checkbox-label align-items-center w-100">
                                                <div class="checkbox-custom-wrapper d-flex w-100 justify-content-center">
                                                    <div class="checkbox-back position-relative w-100">
                                                        <div class="checkbox-mark position-absolute w-100 h-100 d-flex justify-content-center align-items-center">
                                                            <div class="checkbox-title wrap sourceSansPro-regular">
                                                                Privātpersona
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                </div>


                                <div class="checkbox-single w-100">
                                    <div class="d-flex justify-content-between align-items-center w-100">
                                        <div class="privacy-child w-100">
                                            <input type="radio" name="legalStatus" id="legal"
                                                   class="position-absolute">
                                            <label for="legal"
                                                   class="d-inline-flex checkbox-label align-items-center w-100">
                                                <div class="checkbox-custom-wrapper d-flex justify-content-center w-100">
                                                    <div class="checkbox-back position-relative">
                                                        <div class="checkbox-mark position-absolute w-100 h-100 d-flex justify-content-center align-items-center">
                                                            <div class="checkbox-title wrap sourceSansPro-regular">Jur.
                                                                persona
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <!-- Radiobutton END -->

                            </div>
                            <div class="single-input">
                                <input placeholder="Tālruņa numurs" id="name-contacts" type="number">
                            </div>


                            <div class="single-input">
                                <input placeholder="Vārds" id="name-contacts" type="text">
                            </div>


                            <div class="single-input">
                                <input placeholder="Uzvārds" id="surname-contacts" type="text">
                            </div>


                        </div>
                    </div>

                    <div class="w-100">
                        <h2>Preču saņemšanas veids</h2>

                        <!--Radiobutton -->
                        <div class="checkbox-single w-100">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="privacy-child">
                                    <input type="radio" name="radio" id="onPlace"
                                           class="position-absolute ">
                                    <label for="onPlace"
                                           class="d-inline-flex checkbox-label align-items-center">
                                        <div class="checkbox-custom-wrapper d-flex justify-content-center">
                                            <div class="checkbox-back position-relative">
                                                <div class="checkbox-mark position-absolute w-100 h-100 d-flex justify-content-center align-items-center">
                                                    <img src="images/icons/check-mark.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="checkbox-title wrap sourceSansPro-regular">Personīgi Zaļenieku
                                            kokaudzētavā
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>


                        <div class="checkbox-single w-100">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="privacy-child">
                                    <input type="radio" name="radio" id="inCenter"
                                           class="position-absolute">
                                    <label for="inCenter"
                                           class="d-inline-flex checkbox-label align-items-center">
                                        <div class="checkbox-custom-wrapper d-flex justify-content-center">
                                            <div class="checkbox-back position-relative">
                                                <div class="checkbox-mark position-absolute w-100 h-100 d-flex justify-content-center align-items-center">
                                                    <img src="images/icons/check-mark.svg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="checkbox-title wrap sourceSansPro-regular">Rīgas stādu centrā
                                            (Gramzdas iela 33, Rīga, LV-1029)
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <!-- Radiobutton END -->


                        <textarea name="message" placeholder="Piezīmes"></textarea>


                        <input class="button button-black prevent-shaking-animation button-hover-shadow w-100"
                               type="submit" value="Pasūtīt">
                        <div class="position-relative">
                            <img src="images/icons/preloader.png" class="preloader preloader-subscribe position-absolute">
                            <div class="warning-message non-valid-email-warning w-100" style="display: none;">E-pasts nav derīgs</div>
                            <div class="warning-message empty-field-warning w-100" style="display: none;">Nav ievadīta visa nepieciešāmā informācija</div>
                        </div>
                    </div>

                </div>


            </form>

        </div>


    </div>

<?php require_once "footer.php"; ?>